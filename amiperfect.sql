-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Сен 11 2018 г., 11:32
-- Версия сервера: 10.1.33-MariaDB
-- Версия PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `amiperfect`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `test_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `test_name`, `answer`, `text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Chin correction', 'Good', '140', '2018-09-08 07:16:00', '2018-09-10 03:20:05', NULL),
(2, 'Chin correction', 'Bad', '100', '2018-09-10 03:19:18', '2018-09-10 03:19:18', NULL),
(3, 'Chin correction', 'Normal', '120', '2018-09-10 03:19:34', '2018-09-10 03:19:34', NULL),
(4, 'Chin correction', 'Very Good', '160', '2018-09-10 03:19:52', '2018-09-10 03:19:52', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '', 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '', 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '', 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '', 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(49, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 7),
(52, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 8),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, '', 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '', 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '', 12),
(57, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(58, 7, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, NULL, 2),
(59, 7, 'service', 'text', 'Service', 0, 1, 1, 1, 1, 1, NULL, 3),
(60, 7, 'details', 'text', 'Details', 0, 1, 1, 1, 1, 1, NULL, 4),
(61, 7, 'image', 'text', 'Image', 0, 1, 1, 1, 1, 1, NULL, 5),
(62, 7, 'side_image', 'text', 'Side Image', 0, 1, 1, 1, 1, 1, NULL, 6),
(63, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 7),
(64, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 8),
(65, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(66, 10, 'test_name', 'text', 'Test Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(67, 10, 'answer', 'text', 'Answer', 0, 1, 1, 1, 1, 1, NULL, 3),
(68, 10, 'text', 'text', 'Text', 0, 1, 1, 1, 1, 1, NULL, 4),
(69, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(70, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(71, 10, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-08-31 07:57:21', '2018-08-31 07:57:21'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-08-31 07:57:21', '2018-08-31 07:57:21'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-08-31 07:57:21', '2018-08-31 07:57:21'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-08-31 07:57:28', '2018-08-31 07:57:28'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2018-08-31 07:57:28', '2018-08-31 07:57:28'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2018-08-31 07:57:29', '2018-08-31 07:57:29'),
(7, 'user_tests', 'tests', 'User Test', 'User Tests', NULL, 'App\\Models\\UserTest', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-08 06:17:24', '2018-09-08 06:17:24'),
(10, 'answers', 'answers', 'Answer', 'Answers', NULL, 'App\\Models\\Answer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-08 07:14:44', '2018-09-08 07:14:44');

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `original_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `original_name`, `filename`, `created_at`, `updated_at`) VALUES
(1, 'pers.jpg', 'pers', '2018-08-30 03:13:26', '2018-08-30 03:13:26'),
(2, 'Без названия — копия.jpg', 'без-названия-—-копия', '2018-08-30 03:14:21', '2018-08-30 03:14:21'),
(3, 'pers.jpg', 'pers', '2018-08-30 03:17:25', '2018-08-30 03:17:25'),
(4, 'Без названия (2) — копия.jpg', 'без-названия-2-—-копия', '2018-08-30 03:19:03', '2018-08-30 03:19:03'),
(5, '1mb.jpg', '1mb', '2018-08-30 03:21:12', '2018-08-30 03:21:12'),
(6, '1mb.jpg', '1mb', '2018-08-30 03:27:21', '2018-08-30 03:27:21'),
(7, '1mb.jpg', '1mb', '2018-08-30 03:28:13', '2018-08-30 03:28:13'),
(8, 'eu_logo-150x120.png', 'eulogo-150x120', '2018-08-30 03:28:45', '2018-08-30 03:28:45'),
(9, '1mb.jpg', '1mb', '2018-08-30 03:29:20', '2018-08-30 03:29:20'),
(10, '1mb.jpg', '1mb-f0a67', '2018-08-30 03:31:39', '2018-08-30 03:31:39'),
(11, 'Без названия (2) — копия.jpg', 'без-названия-2-—-копия', '2018-08-30 03:32:13', '2018-08-30 03:32:13'),
(12, 'eu_logo-150x120.png', 'eulogo-150x120-0119d', '2018-08-30 03:34:29', '2018-08-30 03:34:29'),
(13, 'Без названия (5) .jpg', 'без-названия-5', '2018-08-30 03:34:35', '2018-08-30 03:34:35'),
(14, '1mb.jpg', '1mb-6948c', '2018-08-30 03:34:49', '2018-08-30 03:34:49'),
(15, 'pers.jpg', 'pers', '2018-08-30 03:35:10', '2018-08-30 03:35:10'),
(16, 'Без названия (3).jpg', 'без-названия-3', '2018-08-30 03:36:13', '2018-08-30 03:36:13'),
(17, '1mb.jpg', '1mb-92796', '2018-08-30 03:36:37', '2018-08-30 03:36:37'),
(18, '1mb.jpg', '1mb-850da', '2018-08-30 03:38:50', '2018-08-30 03:38:50'),
(19, '1mb.jpg', '1mb', '2018-08-30 03:41:23', '2018-08-30 03:41:23'),
(20, '1mb.jpg', '1mb-90374', '2018-08-30 03:43:37', '2018-08-30 03:43:37'),
(21, '1mb.jpg', '1mb-1f485', '2018-08-30 03:48:36', '2018-08-30 03:48:36'),
(22, 'download.jpg', 'download', '2018-08-30 03:50:53', '2018-08-30 03:50:53'),
(23, '1mb.jpg', '1mb-03e9b', '2018-08-30 03:54:15', '2018-08-30 03:54:15'),
(24, '4mb.jpg', '4mb', '2018-08-30 04:00:29', '2018-08-30 04:00:29'),
(25, '1mb.jpg', '1mb-f9a71', '2018-08-30 04:02:26', '2018-08-30 04:02:26'),
(26, '1mb.jpg', '1mb-560fd', '2018-08-30 04:02:32', '2018-08-30 04:02:32'),
(27, '1mb.jpg', '1mb-3306e', '2018-08-30 04:18:55', '2018-08-30 04:18:55'),
(28, '1mb.jpg', '1mb-8e480', '2018-08-30 04:25:31', '2018-08-30 04:25:31'),
(29, 'pers.jpg', 'pers', '2018-08-30 04:25:36', '2018-08-30 04:25:36'),
(30, '1mb.jpg', '1mb-c0d2e', '2018-08-30 04:43:52', '2018-08-30 04:43:52'),
(31, '1mb.jpg', '1mb-1b1fb', '2018-08-30 04:45:00', '2018-08-30 04:45:00'),
(32, '1mb.jpg', '1mb-0e8da', '2018-08-30 04:48:56', '2018-08-30 04:48:56'),
(33, 'pers.jpg', 'pers', '2018-08-30 04:49:16', '2018-08-30 04:49:16'),
(34, '1mb.jpg', '1mb', '2018-08-30 04:52:41', '2018-08-30 04:52:41'),
(35, 'download.jpg', 'download', '2018-08-30 04:55:46', '2018-08-30 04:55:46'),
(36, 'pers.jpg', 'pers', '2018-08-30 04:55:57', '2018-08-30 04:55:57'),
(37, 'teq_person.jpg', 'teqperson', '2018-08-30 04:56:05', '2018-08-30 04:56:05'),
(38, 'person.jpg', 'person', '2018-08-30 04:58:01', '2018-08-30 04:58:01'),
(39, 'eu_logo-150x120.png', 'eulogo-150x120', '2018-08-30 04:58:07', '2018-08-30 04:58:07'),
(40, '1mb.jpg', '1mb-f3e47', '2018-08-30 05:16:26', '2018-08-30 05:16:26'),
(41, '1mb.jpg', '1mb-166cc', '2018-08-30 05:20:21', '2018-08-30 05:20:21'),
(42, '1mb.jpg', '1mb-b88d5', '2018-08-30 05:51:30', '2018-08-30 05:51:30'),
(43, 'pers.jpg', 'pers-be02e', '2018-08-30 05:52:32', '2018-08-30 05:52:32'),
(44, '1mb.jpg', '1mb-2fa7a', '2018-08-30 06:00:42', '2018-08-30 06:00:42'),
(45, 'perso.jpg', 'perso', '2018-08-30 06:05:49', '2018-08-30 06:05:49'),
(46, '1mb.jpg', '1mb', '2018-08-30 06:11:15', '2018-08-30 06:11:15'),
(47, 'perso.jpg', 'perso-9475a', '2018-08-30 06:11:33', '2018-08-30 06:11:33'),
(48, 'perso.jpg', 'perso-ff149', '2018-08-30 06:11:44', '2018-08-30 06:11:44'),
(49, 'perso.jpg', 'perso-12903', '2018-08-30 06:12:46', '2018-08-30 06:12:46'),
(50, 'person.jpg', 'person', '2018-08-30 06:20:52', '2018-08-30 06:20:52'),
(51, 'person.jpg', 'person', '2018-08-30 06:29:24', '2018-08-30 06:29:24'),
(52, 'person.jpg', 'person-34a2b', '2018-08-30 06:30:52', '2018-08-30 06:30:52'),
(53, 'person.jpg', 'person-f9162', '2018-08-30 06:31:37', '2018-08-30 06:31:37'),
(54, 'perso.jpg', 'perso', '2018-08-30 06:36:04', '2018-08-30 06:36:04'),
(55, 'perso.jpg', 'perso-f92ac', '2018-08-30 06:36:47', '2018-08-30 06:36:47'),
(56, 'perso.jpg', 'perso-00efa', '2018-08-30 06:38:54', '2018-08-30 06:38:54'),
(57, 'perso.jpg', 'perso-d521b', '2018-08-30 06:39:29', '2018-08-30 06:39:29'),
(58, 'person.jpg', 'person-f1f24', '2018-08-30 06:39:46', '2018-08-30 06:39:46'),
(59, 'teq_person.jpg', 'teqperson', '2018-08-30 06:39:54', '2018-08-30 06:39:54'),
(60, 'person.jpg', 'person-57434', '2018-08-30 06:40:17', '2018-08-30 06:40:17'),
(61, 'person.jpg', 'person-01855', '2018-08-30 06:46:18', '2018-08-30 06:46:18'),
(62, 'perso.jpg', 'perso-f2332', '2018-08-30 06:46:24', '2018-08-30 06:46:24'),
(63, 'pers.jpg', 'pers', '2018-08-30 06:48:23', '2018-08-30 06:48:23'),
(64, 'perso.jpg', 'perso-cc54e', '2018-08-30 08:29:49', '2018-08-30 08:29:49'),
(65, '4mb.jpg', '4mb', '2018-08-31 02:51:51', '2018-08-31 02:51:51'),
(66, 'person.jpg', 'person-8c28e', '2018-08-31 02:52:05', '2018-08-31 02:52:05'),
(67, 'person.jpg', 'person-57c5f', '2018-08-31 02:53:58', '2018-08-31 02:53:58'),
(68, 'perso.jpg', 'perso-ffa88', '2018-08-31 02:54:08', '2018-08-31 02:54:08'),
(69, 'perso.jpg', 'perso-996ce', '2018-08-31 05:00:14', '2018-08-31 05:00:14'),
(70, '1mb.jpg', '1mb', '2018-09-01 04:29:15', '2018-09-01 04:29:15'),
(71, 'perso.jpg', 'perso-6bb24', '2018-09-01 04:29:40', '2018-09-01 04:29:40'),
(72, 'perso.jpg', 'perso', '2018-09-01 04:31:15', '2018-09-01 04:31:15');

-- --------------------------------------------------------

--
-- Структура таблицы `image_uploads`
--

CREATE TABLE `image_uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-08-31 07:57:22', '2018-08-31 07:57:22');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-08-31 07:57:22', '2018-08-31 07:57:22', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2018-08-31 07:57:22', '2018-09-08 05:23:49', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-08-31 07:57:22', '2018-08-31 07:57:22', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-08-31 07:57:22', '2018-08-31 07:57:22', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2018-08-31 07:57:22', '2018-09-08 05:23:50', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-08-31 07:57:22', '2018-09-08 05:23:50', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-08-31 07:57:22', '2018-09-08 05:23:50', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-08-31 07:57:22', '2018-09-08 05:23:50', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-08-31 07:57:22', '2018-09-08 05:23:50', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2018-08-31 07:57:22', '2018-09-08 05:23:50', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 7, '2018-08-31 07:57:28', '2018-09-08 05:23:50', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2018-08-31 07:57:29', '2018-09-08 05:23:49', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2018-08-31 07:57:30', '2018-09-08 05:23:49', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-08-31 07:57:33', '2018-09-08 05:23:50', 'voyager.hooks', NULL),
(15, 1, 'Tests', '', '_self', 'voyager-news', '#000000', NULL, 10, '2018-09-03 08:22:52', '2018-09-08 05:53:18', 'voyager.tests.index', 'null'),
(18, 1, 'Answers', '', '_self', 'voyager-pen', '#000000', NULL, 11, '2018-09-08 06:32:12', '2018-09-08 06:33:31', 'voyager.answers.index', 'null');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_08_23_135753_create_image_uploads_table', 1),
(5, '2018_08_30_070224_create_images_table', 1),
(6, '2016_01_01_000000_add_voyager_user_fields', 2),
(7, '2016_01_01_000000_create_data_types_table', 2),
(8, '2016_05_19_173453_create_menu_table', 2),
(9, '2016_10_21_190000_create_roles_table', 2),
(10, '2016_10_21_190000_create_settings_table', 2),
(11, '2016_11_30_135954_create_permission_table', 2),
(12, '2016_11_30_141208_create_permission_role_table', 2),
(13, '2016_12_26_201236_data_types__add__server_side', 2),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 2),
(15, '2017_01_14_005015_create_translations_table', 2),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 2),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 2),
(18, '2017_04_21_000000_add_order_to_data_rows_table', 2),
(19, '2017_07_05_210000_add_policyname_to_data_types_table', 2),
(20, '2017_08_05_000000_add_group_to_settings_table', 2),
(21, '2017_11_26_013050_add_user_role_relationship', 2),
(39, '2016_01_01_000000_create_pages_table', 7),
(40, '2016_01_01_000000_create_posts_table', 7),
(41, '2016_02_15_204651_create_categories_table', 7),
(42, '2017_04_11_000000_alter_post_nullable_fields_table', 7),
(43, '2017_11_26_015000_create_user_roles_table', 8),
(44, '2018_03_11_000000_add_user_settings', 8),
(45, '2018_03_14_000000_add_details_to_data_types_table', 8),
(46, '2018_03_16_000000_make_settings_value_nullable', 8),
(48, '2018_09_01_104929_create_user_images_table', 10),
(49, '2018_08_20_072114_create_verify_users_table', 11),
(50, '2018_09_01_125804_create_user_tests_table', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(2, 'browse_bread', NULL, '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(3, 'browse_database', NULL, '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(4, 'browse_media', NULL, '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(5, 'browse_compass', NULL, '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(6, 'browse_menus', 'menus', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(7, 'read_menus', 'menus', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(8, 'edit_menus', 'menus', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(9, 'add_menus', 'menus', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(10, 'delete_menus', 'menus', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(11, 'browse_roles', 'roles', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(12, 'read_roles', 'roles', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(13, 'edit_roles', 'roles', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(14, 'add_roles', 'roles', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(15, 'delete_roles', 'roles', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(16, 'browse_users', 'users', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(17, 'read_users', 'users', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(18, 'edit_users', 'users', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(19, 'add_users', 'users', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(20, 'delete_users', 'users', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(21, 'browse_settings', 'settings', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(22, 'read_settings', 'settings', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(23, 'edit_settings', 'settings', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(24, 'add_settings', 'settings', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(25, 'delete_settings', 'settings', '2018-08-31 07:57:23', '2018-08-31 07:57:23'),
(26, 'browse_categories', 'categories', '2018-08-31 07:57:28', '2018-08-31 07:57:28'),
(27, 'read_categories', 'categories', '2018-08-31 07:57:28', '2018-08-31 07:57:28'),
(28, 'edit_categories', 'categories', '2018-08-31 07:57:28', '2018-08-31 07:57:28'),
(29, 'add_categories', 'categories', '2018-08-31 07:57:28', '2018-08-31 07:57:28'),
(30, 'delete_categories', 'categories', '2018-08-31 07:57:28', '2018-08-31 07:57:28'),
(31, 'browse_posts', 'posts', '2018-08-31 07:57:29', '2018-08-31 07:57:29'),
(32, 'read_posts', 'posts', '2018-08-31 07:57:29', '2018-08-31 07:57:29'),
(33, 'edit_posts', 'posts', '2018-08-31 07:57:29', '2018-08-31 07:57:29'),
(34, 'add_posts', 'posts', '2018-08-31 07:57:29', '2018-08-31 07:57:29'),
(35, 'delete_posts', 'posts', '2018-08-31 07:57:29', '2018-08-31 07:57:29'),
(36, 'browse_pages', 'pages', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(37, 'read_pages', 'pages', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(38, 'edit_pages', 'pages', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(39, 'add_pages', 'pages', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(40, 'delete_pages', 'pages', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(41, 'browse_hooks', NULL, '2018-08-31 07:57:33', '2018-08-31 07:57:33'),
(42, 'browse_user_tests', 'user_tests', '2018-09-08 06:17:24', '2018-09-08 06:17:24'),
(43, 'read_user_tests', 'user_tests', '2018-09-08 06:17:24', '2018-09-08 06:17:24'),
(44, 'edit_user_tests', 'user_tests', '2018-09-08 06:17:24', '2018-09-08 06:17:24'),
(45, 'add_user_tests', 'user_tests', '2018-09-08 06:17:24', '2018-09-08 06:17:24'),
(46, 'delete_user_tests', 'user_tests', '2018-09-08 06:17:24', '2018-09-08 06:17:24'),
(47, 'browse_answers', 'answers', '2018-09-08 07:14:44', '2018-09-08 07:14:44'),
(48, 'read_answers', 'answers', '2018-09-08 07:14:44', '2018-09-08 07:14:44'),
(49, 'edit_answers', 'answers', '2018-09-08 07:14:44', '2018-09-08 07:14:44'),
(50, 'add_answers', 'answers', '2018-09-08 07:14:44', '2018-09-08 07:14:44'),
(51, 'delete_answers', 'answers', '2018-09-08 07:14:44', '2018-09-08 07:14:44');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-08-31 07:57:22', '2018-08-31 07:57:22'),
(2, 'user', 'Normal User', '2018-08-31 07:57:22', '2018-08-31 07:57:22');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-08-31 07:57:30', '2018-08-31 07:57:30'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-08-31 07:57:31', '2018-08-31 07:57:31'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-08-31 07:57:32', '2018-08-31 07:57:32');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `full_name`, `user_name`, `gender`, `birthday`, `email`, `avatar`, `password`, `verified`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sergio', 'Sergio', 1, '2018-08-24', 'erevichkasenc@gmail.com', 'users/default.png', '$2y$10$oi889u1oPXxGmrYHy6QnU.4PSsMHPB31VoA5GJiuD9dtWQRGVmVXG', 1, 'pwjvJUdEpMDWFReR0DiqtxXJvPTj6WOArxSxpxup0fTKghXNdWXEdJYbZXVo', NULL, '2018-08-30 06:44:09', '2018-08-30 06:44:09'),
(4, 2, 'Gevorg Galstyan', 'User', 1, '1989-03-12', 'admin@yourbusiness.am', 'users/default.png', '$2y$10$yQIxWlaohtYJ0uXlmAyYoei7tUy2bpSBTzSIXwJHUEFM2W9AeIOXq', 0, 'WYLNHaozVxtpXPvWmv43fuSDFsu76dplkXAeMPbp8HdhFET1AsfvhIWf2u85', NULL, '2018-08-31 09:16:44', '2018-08-31 09:16:45'),
(6, 2, 'Areg', 'areg1189', 1, '1989-04-11', 'areg.terxazaryan@gmail.com', 'users/default.png', '$2y$10$Wmc9fyAl7lFyL062oixYrOJxCyOmv5RPFhUjM3A7oPSJj80R9uJu.', 1, 'mrGVroxAdbufV0DPpy1z6Js0zutlVwwmhku5VrwKfekUOFk82HkZOJ9Dyq5x', NULL, '2018-09-01 02:58:08', '2018-09-01 03:39:01');

-- --------------------------------------------------------

--
-- Структура таблицы `user_images`
--

CREATE TABLE `user_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_images`
--

INSERT INTO `user_images` (`id`, `user_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 'users\\1\\September2018\\perso.jpg-zg8Aq.jpg', '2018-09-07 07:08:55', '2018-09-07 07:08:55'),
(2, 1, 'users\\1\\September2018\\perso.jpg-y07mC.jpg', '2018-09-07 07:10:54', '2018-09-07 07:10:54'),
(3, 1, 'users\\1\\September2018\\perso.jpg-qDpGP.jpg', '2018-09-07 07:12:35', '2018-09-07 07:12:35'),
(4, 1, 'users\\1\\September2018\\upload_test_side.png.png', '2018-09-07 07:13:02', '2018-09-07 07:13:02'),
(5, 1, 'users\\1\\September2018\\upload_test_side.png-f0kVz.png', '2018-09-07 07:13:56', '2018-09-07 07:13:56'),
(6, 1, 'users\\1\\September2018\\perso.jpg-OjqbR.jpg', '2018-09-07 07:18:32', '2018-09-07 07:18:32'),
(7, 1, 'users\\1\\September2018\\perso.jpg-DRsnZ.jpg', '2018-09-07 07:24:50', '2018-09-07 07:24:50'),
(8, 1, 'users\\1\\September2018\\perso.jpg-9zdpj.jpg', '2018-09-07 07:25:06', '2018-09-07 07:25:06'),
(9, 1, 'users\\1\\September2018\\perso.jpg-h5lDG.jpg', '2018-09-07 07:25:41', '2018-09-07 07:25:41'),
(10, 1, 'users\\1\\September2018\\perso.jpg-WOgX1.jpg', '2018-09-07 07:26:18', '2018-09-07 07:26:18'),
(11, 1, 'users\\1\\September2018\\perso.jpg-IeBMV.jpg', '2018-09-07 07:26:32', '2018-09-07 07:26:32'),
(12, 1, 'users\\1\\September2018\\perso.jpg-L8oET.jpg', '2018-09-07 07:29:11', '2018-09-07 07:29:11'),
(13, 1, 'users\\1\\September2018\\perso.jpg-N6NTw.jpg', '2018-09-07 07:30:45', '2018-09-07 07:30:45'),
(14, 1, 'users\\1\\September2018\\perso.jpg-ZETQq.jpg', '2018-09-07 07:33:12', '2018-09-07 07:33:12'),
(15, 1, 'users\\1\\September2018\\perso.jpg-vfLh7.jpg', '2018-09-07 07:33:56', '2018-09-07 07:33:56'),
(16, 1, 'users\\1\\September2018\\perso.jpg-6uOnW.jpg', '2018-09-07 07:35:46', '2018-09-07 07:35:46'),
(17, 1, 'users\\1\\September2018\\perso.jpg-9mfea.jpg', '2018-09-07 07:38:18', '2018-09-07 07:38:18'),
(18, 1, 'users\\1\\September2018\\perso.jpg-xf7RF.jpg', '2018-09-07 07:38:42', '2018-09-07 07:38:42'),
(19, 1, 'users\\1\\September2018\\perso.jpg-kF8jU.jpg', '2018-09-07 07:49:14', '2018-09-07 07:49:14'),
(20, 1, 'users\\1\\September2018\\perso.jpg-vUm16.jpg', '2018-09-07 07:54:05', '2018-09-07 07:54:05'),
(21, 1, 'users\\1\\September2018\\perso.jpg-VqC1n.jpg', '2018-09-07 08:02:49', '2018-09-07 08:02:49'),
(22, 1, 'users\\1\\September2018\\perso.jpg-I8iNi.jpg', '2018-09-07 08:04:19', '2018-09-07 08:04:19'),
(23, 1, 'users\\1\\September2018\\perso.jpg-OJpr6.jpg', '2018-09-07 08:05:28', '2018-09-07 08:05:28'),
(24, 1, 'users\\1\\September2018\\perso.jpg-jozUP.jpg', '2018-09-07 08:06:14', '2018-09-07 08:06:14'),
(25, 1, 'users\\1\\September2018\\perso.jpg-5Kd8y.jpg', '2018-09-07 08:06:32', '2018-09-07 08:06:32'),
(26, 1, 'users\\1\\September2018\\perso.jpg-4aXq4.jpg', '2018-09-07 08:11:40', '2018-09-07 08:11:40'),
(27, 1, 'users\\1\\September2018\\perso.jpg-J03la.jpg', '2018-09-07 08:12:14', '2018-09-07 08:12:14'),
(28, 1, 'users\\1\\September2018\\perso.jpg-RwcrH.jpg', '2018-09-07 08:13:35', '2018-09-07 08:13:35'),
(29, 1, 'users\\1\\September2018\\perso.jpg-o60if.jpg', '2018-09-07 08:13:59', '2018-09-07 08:13:59'),
(30, 1, 'users\\1\\September2018\\perso.jpg-oa80Y.jpg', '2018-09-07 08:15:16', '2018-09-07 08:15:16'),
(31, 1, 'users\\1\\September2018\\perso.jpg-JoNCh.jpg', '2018-09-07 08:17:33', '2018-09-07 08:17:33'),
(32, 1, 'users\\1\\September2018\\perso.jpg-vRC3l.jpg', '2018-09-07 08:18:59', '2018-09-07 08:18:59'),
(33, 1, 'users\\1\\September2018\\l.jpg.jpg', '2018-09-07 08:19:29', '2018-09-07 08:19:29'),
(34, 1, 'users\\1\\September2018\\perso.jpg-U5aHP.jpg', '2018-09-07 08:23:35', '2018-09-07 08:23:35'),
(35, 1, 'users\\1\\September2018\\perso.jpg-eIv2g.jpg', '2018-09-07 08:24:12', '2018-09-07 08:24:12'),
(36, 1, 'users\\1\\September2018\\perso.jpg-r1lnp.jpg', '2018-09-07 08:24:42', '2018-09-07 08:24:42'),
(37, 1, 'users\\1\\September2018\\perso.jpg-sDqoH.jpg', '2018-09-07 08:25:50', '2018-09-07 08:25:50'),
(38, 1, 'users\\1\\September2018\\perso.jpg-j84qV.jpg', '2018-09-07 08:26:05', '2018-09-07 08:26:05'),
(39, 1, 'users\\1\\September2018\\perso.jpg-U8iUO.jpg', '2018-09-07 08:28:49', '2018-09-07 08:28:49'),
(40, 1, 'users\\1\\September2018\\l.jpg-pxIlz.jpg', '2018-09-07 08:30:28', '2018-09-07 08:30:28'),
(41, 1, 'users\\1\\September2018\\perso.jpg-PgmmQ.jpg', '2018-09-07 08:37:44', '2018-09-07 08:37:44'),
(42, 1, 'users\\1\\September2018\\l.jpg-hmRWT.jpg', '2018-09-07 08:40:26', '2018-09-07 08:40:26'),
(43, 1, 'users\\1\\September2018\\perso.jpg-nYi3C.jpg', '2018-09-07 09:11:42', '2018-09-07 09:11:42'),
(44, 1, 'users\\1\\September2018\\perso.jpg-ga9qi.jpg', '2018-09-07 09:12:59', '2018-09-07 09:12:59'),
(45, 1, 'users\\1\\September2018\\perso.jpg-uP6mh.jpg', '2018-09-07 09:14:52', '2018-09-07 09:14:52'),
(46, 1, 'users\\1\\September2018\\perso.jpg-4Yaoo.jpg', '2018-09-07 09:20:28', '2018-09-07 09:20:28'),
(47, 1, 'users\\1\\September2018\\perso.jpg-WVEKk.jpg', '2018-09-07 09:20:54', '2018-09-07 09:20:54'),
(48, 1, 'users\\1\\September2018\\perso.jpg-191jf.jpg', '2018-09-07 09:21:25', '2018-09-07 09:21:25'),
(49, 1, 'users\\1\\September2018\\perso.jpg-XxBRU.jpg', '2018-09-07 09:27:30', '2018-09-07 09:27:30'),
(50, 1, 'users\\1\\September2018\\perso.jpg-NNjVd.jpg', '2018-09-07 09:28:14', '2018-09-07 09:28:14'),
(51, 1, 'users\\1\\September2018\\perso.jpg-MW16F.jpg', '2018-09-07 09:29:41', '2018-09-07 09:29:41'),
(52, 1, 'users\\1\\September2018\\perso.jpg-pKZJo.jpg', '2018-09-07 09:31:00', '2018-09-07 09:31:00'),
(53, 1, 'users\\1\\September2018\\perso.jpg-u2OUL.jpg', '2018-09-07 09:31:42', '2018-09-07 09:31:42'),
(54, 1, 'users\\1\\September2018\\perso.jpg-kFl3Z.jpg', '2018-09-07 09:32:14', '2018-09-07 09:32:14'),
(55, 1, 'users\\1\\September2018\\perso.jpg-ofMg3.jpg', '2018-09-07 09:37:39', '2018-09-07 09:37:39'),
(56, 1, 'users\\1\\September2018\\perso.jpg-brVUw.jpg', '2018-09-07 09:42:16', '2018-09-07 09:42:16'),
(57, 1, 'users\\1\\September2018\\perso.jpg-C2ewe.jpg', '2018-09-07 09:42:37', '2018-09-07 09:42:37'),
(58, 1, 'users\\1\\September2018\\perso.jpg-zZUkK.jpg', '2018-09-07 09:43:01', '2018-09-07 09:43:01'),
(59, 1, 'users\\1\\September2018\\perso.jpg-nCVH1.jpg', '2018-09-07 09:43:44', '2018-09-07 09:43:44'),
(60, 1, 'users\\1\\September2018\\perso.jpg-bvP22.jpg', '2018-09-07 09:44:36', '2018-09-07 09:44:36'),
(61, 1, 'users\\1\\September2018\\perso.jpg-YC7lI.jpg', '2018-09-07 09:45:10', '2018-09-07 09:45:10'),
(62, 1, 'users\\1\\September2018\\perso.jpg-CiEy4.jpg', '2018-09-07 09:46:10', '2018-09-07 09:46:10'),
(63, 1, 'users\\1\\September2018\\perso.jpg.jpg', '2018-09-07 09:46:45', '2018-09-07 09:46:45'),
(64, 1, 'users\\1\\September2018\\perso.jpg-osRuA.jpg', '2018-09-07 09:46:57', '2018-09-07 09:46:57'),
(65, 1, 'users\\1\\September2018\\perso.jpg-b3U0Y.jpg', '2018-09-07 09:48:07', '2018-09-07 09:48:07'),
(66, 1, 'users\\1\\September2018\\perso.jpg-xYYmS.jpg', '2018-09-07 09:49:16', '2018-09-07 09:49:16'),
(67, 1, 'users\\1\\September2018\\perso.jpg-CHE0k.jpg', '2018-09-07 09:50:57', '2018-09-07 09:50:57'),
(68, 1, 'users\\1\\September2018\\perso.jpg-omWRv.jpg', '2018-09-07 09:51:22', '2018-09-07 09:51:22'),
(69, 1, 'users\\1\\September2018\\perso.jpg-hMWpj.jpg', '2018-09-07 09:52:13', '2018-09-07 09:52:13'),
(70, 1, 'users\\1\\September2018\\perso.jpg-6d9sv.jpg', '2018-09-07 09:52:43', '2018-09-07 09:52:43'),
(71, 1, 'users\\1\\September2018\\perso.jpg-HooGA.jpg', '2018-09-07 09:53:51', '2018-09-07 09:53:51'),
(72, 1, 'users\\1\\September2018\\perso.jpg-8XmUv.jpg', '2018-09-07 09:54:30', '2018-09-07 09:54:30'),
(73, 1, 'users\\1\\September2018\\perso.jpg-6NEaj.jpg', '2018-09-07 09:55:44', '2018-09-07 09:55:44'),
(74, 1, 'users\\1\\September2018\\perso.jpg-t6zNa.jpg', '2018-09-07 09:56:19', '2018-09-07 09:56:19'),
(75, 1, 'users\\1\\September2018\\perso.jpg-RKpXm.jpg', '2018-09-07 09:59:48', '2018-09-07 09:59:48'),
(76, 1, 'users\\1\\September2018\\perso.jpg-fn9fu.jpg', '2018-09-08 03:04:26', '2018-09-08 03:04:26'),
(77, 1, 'users\\1\\September2018\\perso.jpg-SNk72.jpg', '2018-09-08 03:07:55', '2018-09-08 03:07:55'),
(78, 1, 'users\\1\\September2018\\perso.jpg-ver5t.jpg', '2018-09-08 03:10:29', '2018-09-08 03:10:29'),
(79, 1, 'users\\1\\September2018\\perso.jpg-RcACl.jpg', '2018-09-08 03:12:32', '2018-09-08 03:12:32'),
(80, 1, 'users\\1\\September2018\\perso.jpg-poL33.jpg', '2018-09-08 03:13:01', '2018-09-08 03:13:01'),
(81, 1, 'users\\1\\September2018\\perso.jpg-tksiq.jpg', '2018-09-08 03:14:36', '2018-09-08 03:14:36'),
(82, 1, 'users\\1\\September2018\\perso.jpg-POa8V.jpg', '2018-09-08 03:15:35', '2018-09-08 03:15:35'),
(83, 1, 'users\\1\\September2018\\perso.jpg-hZfH3.jpg', '2018-09-08 03:16:00', '2018-09-08 03:16:00'),
(84, 1, 'users\\1\\September2018\\perso.jpg-Rqszr.jpg', '2018-09-08 03:17:04', '2018-09-08 03:17:04'),
(85, 1, 'users\\1\\September2018\\perso.jpg-p6q9i.jpg', '2018-09-08 03:17:28', '2018-09-08 03:17:28'),
(86, 1, 'users\\1\\September2018\\perso.jpg-AjoD4.jpg', '2018-09-08 03:20:14', '2018-09-08 03:20:14'),
(87, 1, 'users\\1\\September2018\\perso.jpg-0BTew.jpg', '2018-09-08 03:23:15', '2018-09-08 03:23:15'),
(88, 1, 'users\\1\\September2018\\perso.jpg-rp2J3.jpg', '2018-09-08 03:24:15', '2018-09-08 03:24:15'),
(89, 1, 'users\\1\\September2018\\perso.jpg-Hijx4.jpg', '2018-09-08 03:24:54', '2018-09-08 03:24:54'),
(90, 1, 'users\\1\\September2018\\perso.jpg-XfxnY.jpg', '2018-09-08 03:25:58', '2018-09-08 03:25:58'),
(91, 1, 'users\\1\\September2018\\perso.jpg-jahBr.jpg', '2018-09-08 03:27:05', '2018-09-08 03:27:05'),
(92, 1, 'users\\1\\September2018\\perso.jpg-9bJT8.jpg', '2018-09-08 03:27:42', '2018-09-08 03:27:42'),
(93, 1, 'users\\1\\September2018\\perso.jpg-OSUdA.jpg', '2018-09-08 03:29:26', '2018-09-08 03:29:26'),
(94, 1, 'users\\1\\September2018\\perso.jpg-1GCLs.jpg', '2018-09-08 03:31:24', '2018-09-08 03:31:24'),
(95, 1, 'users\\1\\September2018\\perso.jpg-169R8.jpg', '2018-09-08 03:32:36', '2018-09-08 03:32:36'),
(96, 1, 'users\\1\\September2018\\perso.jpg-6FT5W.jpg', '2018-09-08 03:33:15', '2018-09-08 03:33:15'),
(97, 1, 'users\\1\\September2018\\perso.jpg-8M9Y8.jpg', '2018-09-08 03:33:36', '2018-09-08 03:33:36'),
(98, 1, 'users\\1\\September2018\\perso.jpg-sUHHQ.jpg', '2018-09-08 03:34:46', '2018-09-08 03:34:46'),
(99, 1, 'users\\1\\September2018\\perso.jpg-HT3rf.jpg', '2018-09-08 03:37:09', '2018-09-08 03:37:09'),
(100, 1, 'users\\1\\September2018\\perso.jpg-5Vmq7.jpg', '2018-09-08 03:39:03', '2018-09-08 03:39:03'),
(101, 1, 'users\\1\\September2018\\perso.jpg-vv7YH.jpg', '2018-09-08 03:39:34', '2018-09-08 03:39:34'),
(102, 1, 'users\\1\\September2018\\perso.jpg-aZn91.jpg', '2018-09-08 03:41:59', '2018-09-08 03:41:59'),
(103, 1, 'users\\1\\September2018\\perso.jpg-gzlJP.jpg', '2018-09-08 03:42:39', '2018-09-08 03:42:39'),
(104, 1, 'users\\1\\September2018\\perso.jpg-KaeRQ.jpg', '2018-09-08 03:43:18', '2018-09-08 03:43:18'),
(105, 1, 'users\\1\\September2018\\perso.jpg-DIX8U.jpg', '2018-09-08 03:45:07', '2018-09-08 03:45:07'),
(106, 1, 'users\\1\\September2018\\perso.jpg-qoCXq.jpg', '2018-09-08 03:49:35', '2018-09-08 03:49:35'),
(107, 1, 'users\\1\\September2018\\perso.jpg-tRmfJ.jpg', '2018-09-08 03:50:56', '2018-09-08 03:50:56'),
(108, 1, 'users\\1\\September2018\\perso.jpg-4KDH3.jpg', '2018-09-08 03:54:00', '2018-09-08 03:54:00'),
(109, 1, 'users\\1\\September2018\\perso.jpg-LdwGl.jpg', '2018-09-08 03:56:50', '2018-09-08 03:56:50'),
(110, 1, 'users\\1\\September2018\\perso.jpg-GZsD7.jpg', '2018-09-08 03:59:20', '2018-09-08 03:59:20'),
(111, 1, 'users\\1\\September2018\\perso.jpg-3KqQF.jpg', '2018-09-08 03:59:30', '2018-09-08 03:59:30'),
(112, 1, 'users\\1\\September2018\\perso.jpg-LqHzg.jpg', '2018-09-08 04:02:39', '2018-09-08 04:02:39'),
(113, 1, 'users\\1\\September2018\\person.jpg.jpg', '2018-09-08 04:52:14', '2018-09-08 04:52:14'),
(114, 1, 'users\\1\\September2018\\l.jpg.jpg', '2018-09-08 04:52:50', '2018-09-08 04:52:50'),
(115, 1, 'users\\1\\September2018\\perso.jpg-BVFEo.jpg', '2018-09-08 04:58:28', '2018-09-08 04:58:28'),
(116, 1, 'users\\1\\September2018\\l.jpg-WpE2T.jpg', '2018-09-08 04:59:02', '2018-09-08 04:59:02'),
(117, 1, 'users\\1\\September2018\\perso.jpg-yV0Ch.jpg', '2018-09-08 05:03:27', '2018-09-08 05:03:27'),
(118, 1, 'users\\1\\September2018\\perso.jpg-DFexY.jpg', '2018-09-08 05:04:11', '2018-09-08 05:04:11'),
(119, 1, 'users\\1\\September2018\\perso.jpg-PeLEU.jpg', '2018-09-08 05:07:31', '2018-09-08 05:07:31'),
(120, 1, 'users\\1\\September2018\\perso.jpg-pfMox.jpg', '2018-09-08 05:08:07', '2018-09-08 05:08:07'),
(121, 1, 'users\\1\\September2018\\perso.jpg-sMjom.jpg', '2018-09-08 05:09:56', '2018-09-08 05:09:56'),
(122, 1, 'users\\1\\September2018\\perso.jpg-D1fxU.jpg', '2018-09-08 05:10:35', '2018-09-08 05:10:35'),
(123, 1, 'users\\1\\September2018\\perso.jpg-ZwoG0.jpg', '2018-09-08 05:11:29', '2018-09-08 05:11:29'),
(125, 1, 'users\\1\\September2018\\perso.jpg-DJbJZ.jpg', '2018-09-08 05:11:59', '2018-09-08 05:11:59'),
(126, 1, 'users\\1\\September2018\\perso.jpg-MrnWx.jpg', '2018-09-08 05:17:02', '2018-09-08 05:17:02'),
(127, 1, 'users\\1\\September2018\\perso.jpg-aHyCu.jpg', '2018-09-08 05:18:59', '2018-09-08 05:18:59'),
(128, 1, 'users\\1\\September2018\\perso.jpg-DCeJ7.jpg', '2018-09-08 05:19:40', '2018-09-08 05:19:40'),
(129, 1, 'users\\1\\September2018\\perso.jpg-w7giH.jpg', '2018-09-08 05:21:01', '2018-09-08 05:21:01'),
(130, 1, 'users\\1\\September2018\\l.jpg-IOdVq.jpg', '2018-09-08 07:18:34', '2018-09-08 07:18:34'),
(131, 1, 'users\\1\\September2018\\l.jpg-2kc2W.jpg', '2018-09-10 03:41:48', '2018-09-10 03:41:48'),
(132, 1, 'users\\1\\September2018\\perso.jpg-mQfkl.jpg', '2018-09-10 03:44:20', '2018-09-10 03:44:20'),
(133, 1, 'users\\1\\September2018\\perso.jpg-9fOGO.jpg', '2018-09-10 03:44:46', '2018-09-10 03:44:46'),
(134, 1, 'users\\1\\September2018\\perso.jpg-j7JpE.jpg', '2018-09-10 03:46:33', '2018-09-10 03:46:33'),
(135, 1, 'users\\1\\September2018\\perso.jpg-ZMCK4.jpg', '2018-09-10 03:47:30', '2018-09-10 03:47:30'),
(136, 1, 'users\\1\\September2018\\upload_test_side.png.png', '2018-09-10 03:47:52', '2018-09-10 03:47:52'),
(137, 1, 'users\\1\\September2018\\perso.jpg-Xvyk9.jpg', '2018-09-10 04:24:03', '2018-09-10 04:24:03'),
(138, 1, 'users\\1\\September2018\\perso.jpg-YITgi.jpg', '2018-09-10 04:27:56', '2018-09-10 04:27:56'),
(139, 1, 'users\\1\\September2018\\perso.jpg-GFQ17.jpg', '2018-09-10 04:29:06', '2018-09-10 04:29:06'),
(140, 1, 'users\\1\\September2018\\perso.jpg-upAK0.jpg', '2018-09-10 04:30:59', '2018-09-10 04:30:59'),
(141, 1, 'users\\1\\September2018\\perso.jpg-BH1PW.jpg', '2018-09-10 04:47:14', '2018-09-10 04:47:14'),
(142, 1, 'users\\1\\September2018\\upload_test_side.png-EmB1W.png', '2018-09-10 04:47:37', '2018-09-10 04:47:37'),
(143, 1, 'users\\1\\September2018\\perso.jpg-fV6jO.jpg', '2018-09-10 04:50:37', '2018-09-10 04:50:37'),
(144, 1, 'users\\1\\September2018\\perso.jpg-MOna7.jpg', '2018-09-10 04:51:55', '2018-09-10 04:51:55'),
(145, 1, 'users\\1\\September2018\\perso.jpg-bT6i5.jpg', '2018-09-10 04:52:58', '2018-09-10 04:52:58'),
(146, 1, 'users\\1\\September2018\\perso.jpg-wDXgm.jpg', '2018-09-10 05:01:00', '2018-09-10 05:01:00'),
(147, 1, 'users\\1\\September2018\\perso.jpg-tFSaI.jpg', '2018-09-10 05:02:00', '2018-09-10 05:02:00'),
(148, 1, 'users\\1\\September2018\\perso.jpg-JTcX2.jpg', '2018-09-10 05:02:14', '2018-09-10 05:02:14'),
(149, 1, 'users\\1\\September2018\\perso.jpg-MxsRG.jpg', '2018-09-10 05:04:48', '2018-09-10 05:04:48'),
(150, 1, 'users\\1\\September2018\\perso.jpg-tu9jt.jpg', '2018-09-10 05:05:35', '2018-09-10 05:05:35'),
(151, 1, 'users\\1\\September2018\\perso.jpg-1Ky80.jpg', '2018-09-10 05:06:03', '2018-09-10 05:06:03'),
(152, 1, 'users\\1\\September2018\\perso.jpg-ISstN.jpg', '2018-09-10 05:07:10', '2018-09-10 05:07:10'),
(153, 1, 'users\\1\\September2018\\perso.jpg-Y7m01.jpg', '2018-09-10 05:07:32', '2018-09-10 05:07:32'),
(154, 1, 'users\\1\\September2018\\perso.jpg-7v6pY.jpg', '2018-09-10 05:08:42', '2018-09-10 05:08:42'),
(155, 1, 'users\\1\\September2018\\perso.jpg-Oe9C0.jpg', '2018-09-10 05:09:56', '2018-09-10 05:09:56'),
(156, 1, 'users\\1\\September2018\\perso.jpg-gGIq7.jpg', '2018-09-10 05:10:16', '2018-09-10 05:10:16'),
(157, 1, 'users\\1\\September2018\\perso.jpg-NmXIK.jpg', '2018-09-10 05:10:52', '2018-09-10 05:10:52'),
(158, 1, 'users\\1\\September2018\\perso.jpg-uK4aV.jpg', '2018-09-10 05:11:21', '2018-09-10 05:11:21'),
(159, 1, 'users\\1\\September2018\\perso.jpg-p1h5i.jpg', '2018-09-10 05:11:45', '2018-09-10 05:11:45'),
(160, 1, 'users\\1\\September2018\\perso.jpg-HqA65.jpg', '2018-09-10 05:12:01', '2018-09-10 05:12:01'),
(161, 1, 'users\\1\\September2018\\perso.jpg-iqniW.jpg', '2018-09-10 05:12:43', '2018-09-10 05:12:43'),
(162, 1, 'users\\1\\September2018\\perso.jpg-g35ir.jpg', '2018-09-10 05:13:29', '2018-09-10 05:13:29'),
(163, 1, 'users\\1\\September2018\\perso.jpg-IBRqw.jpg', '2018-09-10 05:14:30', '2018-09-10 05:14:30'),
(164, 1, 'users\\1\\September2018\\perso.jpg-SSuGd.jpg', '2018-09-10 05:15:15', '2018-09-10 05:15:15'),
(165, 1, 'users\\1\\September2018\\perso.jpg-SWu44.jpg', '2018-09-10 05:15:35', '2018-09-10 05:15:35'),
(166, 1, 'users\\1\\September2018\\perso.jpg-PqiR7.jpg', '2018-09-10 05:16:00', '2018-09-10 05:16:00');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_tests`
--

CREATE TABLE `user_tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `service` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `side_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_tests`
--

INSERT INTO `user_tests` (`id`, `user_id`, `service`, `details`, `image`, `side_image`, `created_at`, `updated_at`) VALUES
(48, 1, 'chin-correction', '{\"facade\":[\"74\",\"83\",\"100\",\"24\"],\"glsupo\":\"134\",\"horizonal\":\"74\",\"labio\":\"172\",\"sulapo\":\"149\"}', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-KEQMs.jpg', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-C7E8W.jpg', '2018-09-08 04:58:32', '2018-09-08 07:18:55'),
(49, 1, 'chin-correction', NULL, 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-bpnJK.jpg', NULL, '2018-09-10 03:41:50', '2018-09-10 03:41:50'),
(50, 1, 'chin-correction', NULL, 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-XDgW5.jpg', NULL, '2018-09-10 03:44:22', '2018-09-10 03:44:22'),
(51, 1, 'chin-correction', NULL, 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-X3z0k.jpg', NULL, '2018-09-10 03:44:49', '2018-09-10 03:44:49'),
(52, 1, 'chin-correction', NULL, 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-z9b1P.jpg', NULL, '2018-09-10 03:46:35', '2018-09-10 03:46:35'),
(53, 1, 'chin-correction', '{\"facade\":[\"98\",\"108\",\"53\",\"65\"],\"glsupo\":\"135\",\"horizonal\":\"154\",\"labio\":\"170\",\"sulapo\":\"93\"}', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-duJv4.jpg', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-YKsnS.jpg', '2018-09-10 03:47:31', '2018-09-10 03:48:49'),
(54, 1, 'chin-correction', '{\"facade\":[\"102\",\"99\",\"57\",\"63\"]}', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-IlzAI.jpg', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-1OfId.jpg', '2018-09-10 04:24:05', '2018-09-10 04:29:15'),
(55, 1, 'chin-correction', '{\"facade\":[\"113\",\"128\",\"62\",\"78\"]}', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-Znbux.jpg', NULL, '2018-09-10 04:31:09', '2018-09-10 04:31:28'),
(56, 1, 'chin-correction', '{\"facade\":[\"100\",\"98\",\"56\",\"70\"]}', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-hMV7c.jpg', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-Ws6Te.jpg', '2018-09-10 04:47:17', '2018-09-10 04:52:59'),
(57, 1, 'chin-correction', '{\"facade\":[\"138\",\"126\",\"69\",\"93\"]}', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-v7NF0.jpg', NULL, '2018-09-10 05:01:13', '2018-09-10 05:01:30'),
(58, 1, 'chin-correction', '{\"facade\":[\"95\",\"109\",\"46\",\"70\"]}', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-TSs3I.jpg', 'users\\1\\tests\\\\chin-correction\\September2018\\chin-correction-DBTYD.jpg', '2018-09-10 05:02:02', '2018-09-10 05:16:02');

-- --------------------------------------------------------

--
-- Структура таблицы `verify_users`
--

CREATE TABLE `verify_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image_uploads`
--
ALTER TABLE `image_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_name_unique` (`user_name`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_images_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Индексы таблицы `user_tests`
--
ALTER TABLE `user_tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_tests_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT для таблицы `image_uploads`
--
ALTER TABLE `image_uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `user_images`
--
ALTER TABLE `user_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT для таблицы `user_tests`
--
ALTER TABLE `user_tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT для таблицы `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_images`
--
ALTER TABLE `user_images`
  ADD CONSTRAINT `user_images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_tests`
--
ALTER TABLE `user_tests`
  ADD CONSTRAINT `user_tests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
