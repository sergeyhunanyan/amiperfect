<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
//    Artisan::call('storage:link');
    return view('home');
})->name('home');


Route::get('/part', function () {
    return view('frontend.image-parts');
})->name('part');

Route::get('/test', function () {
    return view('chin-correction.test');
})->name('test');



Route::post('before-register', 'Auth\RegisterController@before_register');

Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', function () {
        return view('frontend.dashboard');
    })->name('dashboard');
});


/* Admin Route*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::post('answer-change-service', 'AnswerController@change_service')->name('answer.change_service');
    Route::post('answer-store', 'AnswerController@store')->name('voyager.answers.store');
    Route::put('answer-update/{id}', 'AnswerController@update')->name('voyager.answers.update');
});


//

/*UPLOAD ROUTE*/
Route::post('upload', 'MediaController@upload')->name('upload'); // Upload image

Route::post('crop/{service}/{type}', 'MediaController@crop')->name('crop'); // Crop image

Route::delete('delete-image', 'MediaController@destroy')->name('delete-image'); // delete image




Route::group(['middleware' => 'save-data'], function () {
   Route::get('save-date', 'ServiceController@save_data')->name('save_data');
});
Route::group(['middleware' => ['verified','block']], function () {
    /* PROFILE ROUTE */
    Route::resource('profile', 'ProfileController');
    Route::get('profile/service/{service?}/{test_id?}', 'ProfileController@index')->name('profile.service');
    Route::get('profile-verification', 'ProfileController@verification')->name('profile.verification');
    Route::get('resend-email', 'Auth\AuthController@resend')->name('email.resend-email');
    Route::post('change-resend-email', 'Auth\AuthController@change_resend')->name('email.change-resend-email');

});


Route::get('/auth/{id}/{token}', 'Auth\AuthController@check_token')->name('check-token');


/*SERVICES ROUTE*/
Route::group(['prefix' => 'services'], function () {

    Route::get('', 'ServiceController@index')->name('services');
    Route::get('{service}', 'ServiceController@service')->name('services.service');
    Route::get('{service}/{type}/{test_id?}', 'ServiceController@type')->name('service.type');
    Route::put('{service}/{type}/add-data/{test_id?}', 'ServiceController@add_data')->name('service.add_data');
});



Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/home', function (){
    return redirect()->route('home');
});
