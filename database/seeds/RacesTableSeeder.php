<?php

use Illuminate\Database\Seeder;

class RacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('races')->insert([
            [
                'race' => 'Caucasoid (White)',
            ],
            [
                'race' => 'Negroid (Black)',
            ],
            [
                'race' => 'Capoid (Bushmen/Hottentots)',
            ],
            [
                'race' => 'Mongoloid (Oriental/ Amerindian)',
            ],
            [
                'race' => 'Australoid (Australian Aborigine and Papuan)',
            ]
        ]);
    }
}
