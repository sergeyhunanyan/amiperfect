<?php

namespace App\Services;

use App\Models\UserImage;
use App\Models\UserTest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Service
{

    public static function session_details($request, $service, $type)
    {
        $details = json_decode($service->options, true);
        foreach ($details[0][$type]['option'] as $i => $item) {
            if ($item['type'] == 'compare') {
                unset($item['type']);
                foreach ($item as $key => $value) {
                    $data[$i][$key] = $request->data[$i][$key];
                }
            }
        }
        session(['tests.' . $service->slug . '.' . $type . '.details' => $data]);

        return true;


    }

    public static function add_details($user_id)
    {
        if (Auth::check() && Auth::user()->block == 1) {
            return false;
        }
        if (session('tests')) {

            foreach (session('tests') as $key => $item) {
                $test = new UserTest();
                if ($key == 'images') {
                    continue;
                }
                if (isset($item['facade']) && isset($item['facade']['image']) && !empty($item['facade']['image']) && isset($item['facade']['details']) && !empty($item['facade']['details'])) {
                    $test->facade = json_encode($item['facade']);
                }
                if (isset($item['side']) && isset($item['side']['image']) && !empty($item['side']['image']) && isset($item['side']['details']) && !empty($item['side']['details'])) {
                    $test->side = json_encode($item['side']);
                }
                $test->user_id = $user_id;
                $test->complete = 1;
                $test->service = $key;
                $test->save();
            }
            session()->forget('tests');
        }


    }

    public static function edit_details($request,$service, $test, $type, $image){
        if (Auth::check() && Auth::user()->block == 1) {
            return false;
        }
        $details = json_decode($service->options, true);
        foreach ($details[0][$type]['option'] as $i => $item) {
            if ($item['type'] == 'compare') {
                unset($item['type']);
                foreach ($item as $key => $value) {
                    $data[$i][$key] = $request->data[$i][$key];
                }
            }
        }
        $test->{$type} = json_encode(['details' => $data, 'image' => asset('storage/' . $image)]);
        $test->save();
//        session(['tests.' . $service->slug . '.' . $type . '.details' => $data]);
    }

}
