<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as InterventionImage;


class Image
{
    public static function handle($file, $path, $resize_quality = 75)
    {

        $path = $path . DIRECTORY_SEPARATOR . date('FY') . DIRECTORY_SEPARATOR;

        $filename = self::generateFileName($file, $path);

        $image = InterventionImage::make($file);

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();


        $image = $image->encode($file->getClientOriginalExtension(), $resize_quality);

        Storage::disk('public')->put($fullPath, (string)$image, 'public');
        $data['image'] = $image;
        $data['fullPath'] = $fullPath;
        return $data;

    }


    public static function crop($request, $sercice = 'default', $path)
    {



        $image_url = explode('storage', $request['imgUrl']);
        $image_url = str_replace('\\', '/', storage_path('app/public'.$image_url[1]));

        // resized sizes
        $imgW =  (int)$request['imgW'];
        $imgH = (int)$request['imgH'];

        // crop box
        $cropW = $request['width'];
        $cropH = $request['height'];
        // offsets
        $imgX1 = (int) ($request['imgX1']);
        $imgY1 = (int)$request['imgY1'];
        // rotation angle
        $angle = (int)$request['rotation'];

        $image = InterventionImage::make($image_url);

        $image->resize($imgW, $imgH)
            ->rotate(-$angle)
            ->crop($cropW ,  $cropH , -$cropW/2+$imgW/2, $imgY1 );
        $path = $path . DIRECTORY_SEPARATOR . $sercice . DIRECTORY_SEPARATOR . date('FY') . DIRECTORY_SEPARATOR;
        $filename = self::generateFileName('', $path, $sercice);
        $fullPath = $path . $filename . '.jpg';
        Storage::disk('public')->put($fullPath, $image->encode());

        return $fullPath;


    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param $path
     *
     * @return string
     */
    protected static function generateFileName($file = null, $path, $service = 'default')
    {
        $filename = $file ? $file->getClientOriginalName() : $service;
        // Make sure the filename does not exist, if it does, just regenerate
        while (Storage::disk('public')->exists($path . $filename . ($file ? '.' . $file->getClientOriginalExtension() : '.jpg'))) {

            $filename .= '-' . Str::random(5);
        }
        return $filename;
    }

    public static function destroy($image)
    {
        if (Storage::disk('public')->exists($image)) {
            Storage::disk('public')->delete($image);
            return true;
        }
        return false;
    }

    public static function make_bace_64($request, $sercice = 'default', $path){
        $image = InterventionImage::make($request->image);
        $path = $path . DIRECTORY_SEPARATOR . $sercice . DIRECTORY_SEPARATOR . date('FY') . DIRECTORY_SEPARATOR;
        $filename = self::generateFileName('', $path, $sercice);
        $fullPath = $path . $filename . '.jpg';
        Storage::disk('public')->put($fullPath, $image->encode('jpg', 30));
        return $fullPath;
    }

}
