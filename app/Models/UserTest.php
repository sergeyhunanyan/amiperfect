<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    protected $fillable = [
        'user_id','service','details', 'image', 'complete',
    ];

    public static function unfinished($id){
        self::where('user_id',  $id)->where('complete', 0)->delete();
    }

    public function service(){
        return $this->belongsTo(Service::class, 'service', 'slug');
    }
}
