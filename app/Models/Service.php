<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Service extends Model
{
    protected $fillable = [
        'display_name','name','options','image','slug'
    ];

    public function tests(){
        return $this->hasMany(UserTest::class, 'service', 'slug');
    }

    public function answers(){
        return $this->hasMany(Answer::class, 'service_id', 'id');
    }

}
