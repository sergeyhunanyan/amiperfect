<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Answer extends Model
{
    protected $fillable = ['gender', 'details', 'services_id'];

    public function service(){
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
