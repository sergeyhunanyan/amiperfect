<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VerifyUser extends Model
{
    protected $guarded = [];

    public static function clearOld()
    {
        self::where('created_at', '<', Carbon::now()->subMinutes(60)->toDateTimeString())->delete();
    }

    public static function storeCode($user_id, $email, $token)
    {
        self::clearOld();
        $countFreshSended = self::where(['user_id' => $user_id, 'email' => $email])->where('created_at', '>', Carbon::now()->subMinutes(5)->toDateTimeString())->count();
        if ($countFreshSended == 1) return false;
        self::where('user_id', $user_id)->delete();
        self::create(['user_id' => $user_id, 'email' => $email, 'token' => $token]);
        return true;
    }

    public static function checkToken ($user_id, $token){ // Проверяет код на правильность
        self::clearOld();
        $email_token = self::where(['user_id'=>$user_id, 'token'=>$token])->first();
        if ($email_token) {
            $email_token->delete();
            return true;
        }
        return false;
    }

}
