<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class UniqueVerify implements Rule
{

    private $table;

    private $verify_column;


    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($table, array $verify_column = [])
    {
        $this->table = $table;
        $this->verify_column = $verify_column;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (DB::table($this->table)->where($attribute, $value)->where($this->verify_column)->first()){
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique');
    }
}
