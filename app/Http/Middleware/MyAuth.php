<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MyAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Auth::guest()) {
            if (Auth::user()->verified == 1) {
                if ($request->url() == route('profile.verification')){
                    return redirect()->route('profile.index');
                }
                return $next($request);
            }elseif ($request->url() == route('profile.verification') || $request->url() == route('email.resend-email') || $request->url() == route('email.change-resend-email')){
                return $next($request);
            }
            return redirect()->route('profile.verification');
        }
        return back();
    }
}
