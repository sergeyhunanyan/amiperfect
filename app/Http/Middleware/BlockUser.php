<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BlockUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->block == 1) {
                return redirect()->route('home')->with([
                    'message' => __('auth.blocked'),
                    'alert-type' => 'error'
                ]);
            }
            return $next($request);
        }
        return back()->with([
            'message' => __('auth.required_register'),
            'alert-type' => 'warning'
        ]);
    }
}
