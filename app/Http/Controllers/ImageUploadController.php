<?php

namespace App\Http\Controllers;

use App\Models\ImageUpload;
use Illuminate\Http\Request;
use Softon\LaravelFaceDetect\Facades\FaceDetect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
//        $image = $request->image;
//        $hash = $request->image->hashName();
//
//        $fileArray = array('image' => $image);
//
//        $rules = array(
//            'image' => 'mimes:jpg,jpeg,png,gif|required|max:2048'
//        );
//
//        $validator = Validator::make($fileArray, $rules);
//
//        if ($validator->fails())
//        {
//            return Redirect::back()->withErrors($validator);
//        } else
//        {
//            Storage::disk('public')->put('crop/', $image, 'public');
//            $storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
//            $imgPath = $storagePath . "crop/". $hash;
//            $check_person = FaceDetect::extract($imgPath)->face;
//
//            if($check_person){
//                $img_table = new ImageUpload();
//                $img_table->user_id = $request->user()->id;
//                $img_table->image = $imgPath;
//                $img_table->save();
//
//            }
//            else{
//                Storage::delete('/public/crop/' . $hash);
//                return redirect('image')->with('status', 'It\'s not a person' );
//            }
//        };
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function show(ImageUpload $imageUpload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageUpload $imageUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageUpload $imageUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageUpload $imageUpload)
    {
        //
    }
}
