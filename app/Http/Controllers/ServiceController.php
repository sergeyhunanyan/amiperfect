<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\UserTest;
use App\Services\Image;
use Illuminate\Http\Request;
use App\Services\Service as ServiceTrait;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{


    public function index()
    {
        return view('frontend.services');
    }


    public function service($service)
    {
        Service::where('slug', $service)->firstOrFail();
        return view($service . '.index', compact('service'));
    }

    public function type($service, $type, $test_id = null)
    {
        if ($test_id) {
            $test_id = (explode('-', $test_id))[1];
            if (Auth::check()) {
                $test = UserTest::findOrFail($test_id);
                if ($test->user_id != Auth::id() || $test->service != $service) return abort(404);
            } else return abort(404);

        } else $test = null;
        $types = ['facade', 'side', 'important'];
        if (!in_array($type, $types)) {
            return abort(404);
        }
        Service::where('slug', $service)->firstOrFail();

        return view($service . '.' . $type, compact('service', 'type', 'test'));
    }


    public function add_data(Request $request, $service, $type)
    {
        if ($request->test_id) {
            $test_id = (explode('-', $request->test_id))[1];
            if (Auth::check()) {
                $test = UserTest::findOrFail($test_id);
                if ($test->user_id != Auth::id()) return abort(404);
            } else return abort(404);
        } else $test = null;

        $row_service = Service::where('slug', $service)->firstOrFail();
        $image = Image::make_bace_64($request, $request->service, 'users/tests/');
        if ($test == null) {
            ServiceTrait::session_details($request, $row_service, $type);
            session(['tests.' . $service . '.' . $type . '.image' => asset('storage/' . $image)]);
            if ($request->reg_url) {
                return redirect($request->reg_url);
            }
        }

        if (Auth::check()) {
            if ($test) {
                ServiceTrait::edit_details($request, $row_service, $test, $type, $image);
            } else {
                ServiceTrait::add_details(Auth::id());
            }
            return redirect()->route('profile.service', [
                'service' => $service,
                'test_id?' => $test?'test-'.$test->id:null
            ]);
        } else {
            return back()->with([
                'alert-type' => 'info',
                'message' => __('auth.login_or_register'),
            ]);
        }

    }

    public function important($service)
    {
        dd(1);
    }


}
