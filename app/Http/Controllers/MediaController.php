<?php

namespace App\Http\Controllers;

use App\Models\UserImage;
use App\Models\UserTest;
use Softon\LaravelFaceDetect\Facades\FaceDetect;
use App\Services\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MediaController extends Controller
{
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'img' => 'required|image|mimes:jpeg,jpg,png,gif',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->messages()->first(),
            ], 200);
        }
        $data = Image::handle($request->img, 'users\\images\\');
//            $create_image = UserImage::create([
//                'user_id' => Auth::id(),
//                'image' => $data['fullPath']
//            ]);
//            if ($create_image) {

        session()->push('tests.images', $data['fullPath']);
        $image = $data['image'];
        return response()->json([
            'status' => 'success',
            'url' => asset('storage/' . $data['fullPath']),
//                    'image' => $create_image->id,
            'width' => $image->width(),
            'height' => $image->height()
        ], 200);
//            }
//        return response()->json([
//            'status' => 'error',
//            'message' => __('error.unexplained_error'),
//        ], 200);
    }


    public function crop(Request $request)
    {
        $image = Image::crop($request->all(), $request->service, 'users/tests/');
        if (!$image) {
            return response()->json([
                'status' => 'error',
                'message' => 'Server error while uploading',
            ], 200);
        }


        $type = $request->type;
        $service = $request->service;



        session(['tests.' . $service. '.' . $type .'.image' => asset('storage/' . $image)]);
        return response()->json([
            'status' => 'success',
            'url' => asset('storage/' . $image),
        ], 200);

    }

    public function destroy(Request $request)
    {
        $image = UserImage::findOrFail($request->img);
        if (Image::destroy($image->image)) {
            $image->delete();
        }
    }
}
