<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $services;

    public function __construct()
    {
        $services = Service::get();
        $this->services = $services;
        View::share(compact('services'));
    }
}
