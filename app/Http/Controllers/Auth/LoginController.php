<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Service;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    protected $maxLoginAttempts = 3;
    protected $lockoutTime = 300;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {

        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 5
        );
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }

    public function checkBlock(Request $request){
        $user = User::where('email',$request->email)->get();
        foreach ($user as $res){
            $hashedPassword = $res->password;
            $block = $res->block;
        }
        if (Hash::check($request->password, $hashedPassword))
        {

            return true;
        }
        return false;
    }

    public function login(Request $request)
    {
//        if (Auth::attempt(array('email' => $request->email, 'password' => $request->password) , (Input::get('remember') == 'on') ? true : false) ) {
//            return Redirect::back();
//        }else{
//            return redirect()->route('login')->with('warning', __('auth.failed'));
//        }

        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if ($validate->fails()) {
            if ($request->ajax()) {
                return response()->json(['errors' => $validate->messages()]);
            } else {
                return back()->with([
                    'message' => __('auth.failed'),
                    'alert-type' => 'warning'
                ]);
            }
        }
//        $user = $this->guard()->getLastAttempted();

//        if (Auth::attempt($this->credentials($request), $request->filled('remember'))) {

        if (Auth::attempt($this->credentials($request), $request->filled('remember'))) {
            $this->redirectTo = url()->previous();
            if ($request->ajax()) {
                return response()->json([
                    'success' => true,
                ]);
            }
            return redirect(session('next_url', $this->redirectTo));
        }else{
            if ($request->ajax()) {
                return response()->json([
                    'error' => true,
                    'message' => __('auth.failed'),
                ]);
            }
            return back()->with([
                'message' => __('auth.failed'),
                'alert-type' => 'error'
            ]);
        }


//        if (!$request->ajax()) {
//            $this->redirectTo = url()->previous();
//            // If the class is using the ThrottlesLogins trait, we can automatically throttle
//            // the login attempts for this application. We'll key this by the username and
//            // the IP address of the client making these requests into this application.
//            if ($this->hasTooManyLoginAttempts($request)) {
//                $this->fireLockoutEvent($request);
//                return $this->sendLockoutResponse($request);
//            }
//            if (!$this->checkCredential($request)) {
//                // If the login attempt was unsuccessful we will increment the number of attempts
//                // to login and redirect the user back to the login form. Of course, when this
//                // user surpasses their maximum number of attempts they will get locked out.
//                $this->incrementLoginAttempts($request);
//
//                return $this->sendFailedLoginResponse($request);
//            }
//            $user = $this->guard()->getLastAttempted();
//
//            // If user is confirmed we make the login and delete session information if needed
//            $this->attemptLogin($request);
//            if ($request->session()->has('user_id')) {
//                $request->session()->forget('user_id');
//            }
//            return $this->sendLoginResponse($request);
//
//
//        }
    }

    protected function checkCredential($request)
    {
        return $this->guard()->validate($this->credentials($request));
    }
}
