<?php

namespace App\Http\Controllers\Auth;

use App\Models\Race;
use App\Rules\UniqueVerify;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Services\Service;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'email.unique' => __('validation.user_exist')
        ];
        return Validator::make($data, [
            'user_name' => 'required|string|max:40',
            'gender' => 'required|string',
            'birthday' => 'required|integer|numeric|min:5|max:100',
            'race_name' => 'required|integer|numeric|min:1|exists:races,id',
            'agree' => 'required',
        ], $messages);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->user_name = $data['user_name'];
        $user->gender = $data['gender'];
        $user->birthday = $data['birthday'];
        $user->email = session('reg.email');
        $user->password = bcrypt(session('reg.pass'));
        $user->race = $data['race_name'];
        $user->save();
        Service::add_details($user->id);
        session()->forget('reg');
        session()->forget('tests');
        $this->redirectTo == route('profile.index');
        return $user;

    }

//    public function register(Request $request)
//    {
//        $this->validator($request->all())->validate();
//
//        DB::beginTransaction();
//        try {
//            $user = $this->create($request->all());
//            Auth::login($user);
//
//            $token = str_random(32);
//            VerifyUser::storeCode(Auth::id(), Auth::user()->email, $token);
//
//          Mail::to($user->email)->send(new VerifyMail($user, $token));
//            DB::commit();
//            return redirect()->route('profile.index')->with([
//                'message' => __('success.send_success',['email' => $user->email]),
//                'alert-type' => 'success',
//            ]);
//
//        } catch (\Exception $e) {
//            Auth::logout();
//            DB::rollBack();
//            return back()->with([
//                'message' => __('errors.unexplained_error'),
//                'alert-type' => 'warning',
//            ])->withInput();
//        }
//    }
//
//    public function verifyUser($token)
//    {
//        $verifyUser = VerifyUser::where('token', $token)->first();
//        if (isset($verifyUser)) {
//            $user = $verifyUser->user;
//            $verifyUser->user->verified = 1;
//            $user->save();
//            Auth::login($user);
//            $status = __('generic.welcome'); // Welcome
//            $verifyUser->delete();
//            return redirect()->route('dashboard')->with('status', $status);
//        } else {
//            abort(404);
//        }
//    }
//
//    protected function registered()
//    {
//        $this->guard()->logout();
//        return back()->with('status', __('generic.resend'));
//    }


    public function before_register(Request $request)
    {
        $message = [
            'email.unique' => __('auth.failed'),
            'email.required' => __('auth.required_register'),
            'password.required' => __('auth.required_register'),
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:40|unique:users',
            'password' => 'required|min:6'
        ], $message);
        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json([
                    'errors' => $validator->messages()->first(),
                ]);
            }
        }
        session([
            'reg.email' => $request->email,
            'reg.pass' => $request->password,
        ]);
        return response()->json([
            'success' => true,
            'redirect_ulr' => '/register',
        ]);
    }

    public function showRegistrationForm()
    {
        $races = Race::all();
        return view('auth.register',compact('races'));
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect(route('profile.index'));
    }


}
