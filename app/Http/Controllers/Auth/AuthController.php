<?php

namespace App\Http\Controllers\Auth;

use App\Mail\VerifyMail;
use App\Models\VerifyUser;
use App\User;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    public function resend()
    {
        $token = str_random(32);
        $result = VerifyUser::storeCode(Auth::id(), Auth::user()->email, $token);
        if ($result) {
            try {
                Mail::to(Auth::user()->email)->send(new VerifyMail(Auth::user(),$token));
                return back()->with([
                    'message' => __('success.send_success',['email' => Auth::user()->email]),
                    'alert-type' => 'success',
                ]);
            } catch (\Exception $e) {
                return back()->with([
                    'message' => __('errors.unexplained_error'),
                    'alert-type' => 'warning',
                ]);
            }
        }
        return back()->with([
            'message' => __('errors.get_the_code'),
            'alert-type' => 'warning',
        ]);
    }

    public function change_resend(Request $request){
        $validate = Validator::make($request->all(),[
           'email' => ['required', Rule::unique('users')->where('verified', 0)->ignore(Auth::id())]
        ]);
        if ($validate->fails()){
            if ($request->ajax()){
                return response()->json(['errors' =>$validate->messages()]);
            }
            return back()->withErrors($validate->errors())->withInput();
        }

        if (!$request->ajax()){
            $user = Auth::user();
            $user->email = $request->email;
            $user->save();
            return $this->resend();
        }
    }



    public function check_token(Request $request){
        if (VerifyUser::checkToken($request->id,$request->token)){
            $user = User::findOrFail($request->id);
            $user->verified = 1;
            $user->save();
            Auth::login($user);
            return redirect()->route('profile.index')->with([
                'message' => __('success.email_verified'),
                'alert-type' => 'success'
            ]);
        }
        return back()->with([
            'message' => __('error.invalid_token'),
            'alert-type' => 'warning'
        ]);
    }

}
