<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\UserTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $service = $this->services->where('slug', $request->service ?? $this->services->first()->slug)->first();
        if (!$service) {
            return abort(404);
        }
        $user_info = $service->tests->where('user_id', Auth::id());
        if ($request->test_id) {
            $test_id = (explode('-', $request->test_id))[1];
            $test = UserTest::findOrFail($test_id);
            if ($test->user_id != Auth::id()) return abort(404);
        } else {
            $test_id = null;
        }
        return view('profile.index', compact('user_info', 'test_answers', 'service', 'test_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verification()
    {
        return view('profile.verification');
    }
}
