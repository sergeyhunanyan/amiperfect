<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AnswerController extends Controller
{
    public function index($id)
    {
        $answer = Answer::findOrFail($id);
        return view('vendor.voyager.answers.' . $answer->service_name, compact('answer'));
    }

    public function change_service(Request $request)
    {
        $service = Service::findOrFail($request->service);
        $details = json_decode($service->options, true)[0];
        $answer = Answer::find($request->answer);
        $params = isset($answer->details)? json_decode($answer->details,true): NULL;

        return View::make('vendor.voyager.answers.service-content', compact('details','params'));
    }
    public function store(Request $request){
        if (!$request->ajax()){
            $jsonData = json_encode($request->details);
            $service_id = $request->service_id;

            $answer = new Answer();
            $answer->gender = $request->gender;
            $answer->service_id = $service_id;
            $answer->details =  $jsonData;
            $answer->save();

            return redirect()
                ->route("voyager.answers.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_added_new')." Answers",
                    'alert-type' => 'success',
                ]);
        }

    }

    public function update(Request $request, $id){
        $jsonData = json_encode($request->details);
        $service_id = $request->service_id;

        $answer = Answer::find($id);
        $answer->gender = $request->gender;
        $answer->service_id = $service_id;
        $answer->details =  $jsonData;
        $answer->save();

        return redirect()
            ->route("voyager.answers.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated')." Answers",
                'alert-type' => 'success',
            ]);
    }

}
