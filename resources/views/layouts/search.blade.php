<div class="row justify-content-end align-items-center px-3 text-md-right order-2">
    <div class="header__search__bar col-md-8 hidden-xs d-none d-md-block">
        <form action="">
            <input type="search" name="search" class="border-0"
                   placeholder="search for procedure and professionals">
            <button class="bg-transparent border-0"><i class="fas fa-search"></i></button>
        </form>
    </div>
    <div class="header__soc__icons col-md-4 text-right ">
        <a href="" class="mr-2"><i class="far fa-envelope"></i></a>
        <a href="" class="mr-2"><i class="icon ion-logo-youtube"></i></a>
        <a href="" class="mr-2"><i class="fab fa-facebook-f"></i></a>
        <a href="" class="mr-2"><i class="fab fa-instagram"></i></a>
    </div>
</div>