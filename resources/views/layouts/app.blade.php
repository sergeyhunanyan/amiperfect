<!DOCTYPE html>
<html lang="en">
<head>
    <title>AM|PERFECT</title>
    <!--======= META =======-->
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--======= GOOGLE FONTS ========-->
    <link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">


    <!--======= ICON ========-->
    <link rel="icon" href="{{ asset('storage/img/logo.png') }}" type="image/x-icon">
    <!--======= FONT GOOGLE ========-->
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

    <!--======= FONT AWESOME ========-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!--======= BOOTSTRAP ========-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{ asset('css/et-line.css') }}"/>
    <!--======= ION ICONS =======-->
    <link href="https://unpkg.com/ionicons@4.3.0/dist/css/ionicons.min.css" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <!--======= OWN CSS =======-->
    @yield('style')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/media.css') }}"/>
</head>
<body>

@yield('content')

@guest
    <!-- =================== LOGIN MODAL ======================= -->
    @include('partials.login-modal')
@endguest

<!--======= SCRIPTS =======-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--======= BOOTSTRAP =======-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
        integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!--======= OWN JS =======-->
{{--@section("smoot")--}}
{{--<script src="{{ asset('js/smoothScroll.js') }}"></script>--}}
{{--@show--}}
<script>
    @if(session('message'))

    /* TODO: change Controllers to use AlertsMessages trait... then remove this*/
    var alertType = <?php echo json_encode(Session::get('alert-type', 'info')); ?>;
    var alertMessage = <?php echo json_encode(Session::get('message')); ?>;
    var alerter = toastr[alertType];

    if (alerter) {
        alerter(alertMessage);
    } else {
        toastr.error("toastr alert-type " + alertType + " is unknown");
    }

    @endif
    @if (count($errors) > 0)
    @foreach ($errors->all() as $error)
    toastr.error("{{$error}}");
    @endforeach
    @endif
</script>

@if(Request::url() == route('home'))
    <script>
        $('html,body').smoothScroll();
        underHeader = $('.under__header').offset().top;
        headerSocIcons = $('.navbar__menu').offset().top;
        headerSocIconsHeight = $('.navbar__menu').height();
        $(window).scroll(function () {
            if ($(window).width() > 567) {
                if ($(window).scrollTop() >= underHeader) {
                    $('.under__header').addClass('fixed__header')
                } else {
                    $('.under__header').removeClass('fixed__header')
                }
            } else {
                if ($(window).scrollTop() >= headerSocIcons + headerSocIconsHeight) {
                    $('.under__header').addClass('fixed__header')
                } else {
                    $('.under__header').removeClass('fixed__header')
                }
            }

        })
    </script>
@else
    <script src="{{ asset('js/init.js') }}"></script>
@endif
<script> var test_form = false; </script>
@yield('script')
<script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>
