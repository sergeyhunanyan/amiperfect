@if(Route::current()->getName() == 'home')
    <nav class="navbar__menu">
        <ul class="d-flex d-md-block justify-content-around">
            <li class="navbar__items"><a href="">about</a></li>
            <li class="navbar__items"><a href="{{ route('services') }}">services</a></li>
            <li class="navbar__items"><a href="">tutorials</a></li>
            <li class="navbar__items"><a href="">partnership</a></li>
            <li class="navbar__items"><a href="">professionals</a></li>
        </ul>
    </nav>
@else
    <nav class="navbar__menu">
        <ul class="d-flex  justify-content-center">
            <li class="navbar__items mr-md-2 mr-1"><a href="">about</a></li>
            <li class="navbar__items mr-md-2 mr-1"><a href="{{ route('services') }}">services</a></li>
            <li class="navbar__items mr-md-2 mr-1"><a href="">tutorials</a></li>
            <li class="navbar__items mr-md-2 mr-1"><a href="">partnership</a></li>
            <li class="navbar__items mr-md-2 mr-1"><a href="">professionals</a></li>
        </ul>
        @if(Auth::check())
            <ul class="d-flex mr-auto justify-content-center">
                <li class="navbar__items mr-md-2 mr-1">
                    <a href="{{route('profile.index')}}">Profile</a>
                </li>
                <li class="navbar__items mr-md-2 mr-1">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        @else
            <ul class="d-flex mr-auto justify-content-center">
                <li class="navbar__items mr-md-2 mr-1"><a href="" data-toggle="modal" data-target="#modal-login">Login</a>
                </li>
                {{--<li class="navbar__items mr-md-2 mr-1"><a href="{{ route('register') }}">register</a></li>--}}
            </ul>
        @endif
    </nav>
@endif
