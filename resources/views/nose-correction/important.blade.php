@extends('layouts.app')

@section('style')

@stop

@section('content')
    @include('layouts.navbar')

    <!-- =================== SERVICES ======================= -->
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="logo correct__logo col-lg-2 col-4">
                <a href="/">
                    <img src="{{asset('storage/img/logo.png')}}" width="100%" alt="" class="">
                </a>
            </div>
            <div class="col-12 col-lg-6">
                <h1 class="text-center mb-0 mt-3">
                    <span class="d-inline-block  position-relative chin__correction__name">Nose Correction</span>
                </h1>
            </div>
            <div class="col-12 col-lg-4">
                <h1 class="text-center mb-0 mt-3">
                    <span class="d-inline-block  position-relative border-radius-0 bg-transparent chin__correction__name">Important Points</span>
                </h1>
            </div>
        </div>
    </div>
    {{--FIRST--}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7">
                <h4>
                    <b>
                        The bony base width should be 80% of the normal alar base width.
                        A normal alar base width is equal to the intercanthal distance or the width of one eye.

                    </b>
                </h4>
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <img src="{{asset('storage/img/nose-correction/f.png')}}" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-4">
                        <p class="circle__center">
                            In the frontal view, the nose is analyzed in general facial proportions
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div><img src="{{asset('storage/img/nose-correction/f-2.png')}}" alt="" class="img-fluid"></div>
                <div>
                    <p>
                       <b>*Distance from the Alar base (end of nose) to lip crease(stomion) =1/3 of The Lower Third. More diteils in</b>
                        <a href="{{route('services.service',['lip-injection'])}}" class="btn own__btn">Lip Injection</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="container-fluid">--}}
        {{--<div class="row align-items-center">--}}
            {{--<div class="start__section d-flex align-items-center justify-content-center">--}}
                {{--<div class="text-center px-3 col-6 col-sm order-2 order-sm-1"><a href="" class="own__btn px-4 py-1">Tutorial</a></div>--}}
                {{--<div class="text-center px-3 col-12 col-sm order-1 order-sm-2"><h2>Questionnaire For You</h2></div>--}}
                {{--<div class="text-center px-3 col-6 col-sm order-3 "><a href="{{ route('service.type',['service' => $service, 'type' => 'facade'])}}" class="own__btn px-4 py-1">Start</a></div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
    {{--SECOND--}}
    <div class="container-fluid">
        <div class="row align-items-baseline">
            <object data="{{asset('storage/img/nose-correction/important/nose-2.svg')}}" width="100%" style='font-family: "Pacifico", cursive'></object>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="start__section d-flex align-items-center justify-content-center">
                <div class="text-center px-3 col-6 col-sm order-2 order-sm-1"><a href="" class="own__btn px-4 py-1">Tutorial</a></div>
                <div class="text-center px-3 col-12 col-sm order-1 order-sm-2"><h2>Questionnaire For You</h2></div>
                <div class="text-center px-3 col-6 col-sm order-3 "><a href="{{ route('service.type',['service' => $service, 'type' => 'facade'])}}" class="own__btn px-4 py-1">Start</a></div>
            </div>
        </div>

    </div>
    {{--THIRD--}}
    <div class="container-fluid">
        <div class="row align-items-baseline">
            <object data="{{asset('storage/img/nose-correction/important/nose-3.svg')}}" width="100%" style='font-family: "Pacifico", cursive'></object>
        </div>
    </div>
@endsection

@section('script')

@stop
