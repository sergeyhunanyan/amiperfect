@extends('layouts.app')

@section('content')

    <!-- =================== NAVIGATION ======================= -->
    <header>
        <div class="container-fluid d-flex d-md-block flex-column">

            @include('layouts.search')
            <div class="row justify-content-center my-3">

            </div>
            @include('layouts.navbar')

        </div>
        <div class="container-fluid mt-sm-4 mt-3">
            <div class="row pl-md-3   justify-content-lg-start justify-content-center under__header">
                <div class="col-12 col-lg-8">
                    <div class="row justify-content-around">
                        <div class="logo correct__logo col-lg-3 col-4">
                            <a href="/">
                                <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="" class="">
                            </a>
                        </div>
                        <h1 class="text-center mb-0 mt-3 col-lg-9">
                            <span class="d-inline-block  position-relative chin__correction__name">Nose Correction</span>
                        </h1>
                    </div>
                    <div>
                        <img src="{{ asset('storage/img/nose-correction/fon.jpg') }}" class="img-fluid" alt=""/>
                    </div>

                </div>
                <div class="col-lg-4 col-10 mt-lg-0 mt-4">
                    <p class="w-100 on__image__top text-underline">
                        Because of nose location in the center of the face and it is the key facial feature and
                        therefore crucial in defining our physical identity, most patients requesting this procedure. If
                        you have always been concerned about the appearance of your nose, in order to optimize your
                        overall facial harmony, this OPTIONS are for YOU!
                    </p>
                    <div class="row w-100 justify-content-around">
                        <div class="col-md-4 col-6 text-part-service-block">
                            <a href="">
                                <div class="text-part-service">Enter your <br> measurements</div>
                            </a>
                        </div>
                        <div class="col-md-4 col-6 text-part-service-block">
                            <a href="{{ route('service.type',['service' => $service, 'type' => 'facade'])}}">
                                <div class="text-part-service">Download <br> pictures</div>
                            </a>
                        </div>
                        <div class="col-md-4 col-6 mt-3 mt-md-0 text-part-service-block">
                            <a href="{{ route('service.type',['service' => $service, 'type' => 'important']) }}">
                                <div class="text-part-service">Important <br> points</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p class="on__image__top nose__bottom__text">
                        *A nonsurgical nose job involves placing temporary fillers in the nose area for augmentation, and performed with dermal filler, but result is temporary .

                        <br>
                        *Surgicla rhinoplasty is a permanent surgery with recovery and post-operative time.
                    </p>
                </div>
            </div>
        </div>
    </header>
    <!-- =================== SERVICES ======================= -->
@endsection
