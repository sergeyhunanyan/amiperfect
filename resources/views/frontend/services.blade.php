@extends('layouts.app')

@section('content')
    <!-- =================== NAVIGATION ======================= -->
    <header>
        <div class="container-fluid d-flex d-md-block flex-column">

            @include('layouts.search')
            <div class="row justify-content-center my-3">
                <div class="col">
                    <h1 class="text-center">
                        <span class="d-inline-block part__underline position-relative">SERVICES</span>
                    </h1>
                </div>
            </div>
            @include('layouts.navbar')

        </div>
        <div class="container-fluid mt-sm-4 mt-3">
            <div class="row pl-md-3  align-items-md-center justify-content-center under__header">
                <div class="logo col-md-2 col-4">
                    <a href="/">
                        <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="">
                    </a>

                </div>
                <div class="heading__text heading__text__services col-md-10 col-12 d-md-block d-flex align-items-end">
                    <h1 class="registration text-center">Do You want to now important concept about your face appearance
                        :
                        <br>
                        <span class="text-underline">symmetry,</span> <span class="text-underline">proportion,</span>
                        <span class="text-underline">length and width</span>?
                    </h1>
                </div>
            </div>
        </div>
    </header>
    <!-- =================== SERVICES ======================= -->
    <section id="section-service" class="mt-3 ">
        <h2 class="text-center question__head mb-3 questions">Asking Question :</h2>
        <div class="container">
            <div class="row justify-content-between questions">
                <div class="col-md-6"><h3 class="text-shadow text-md-left text-right">HOW can I make my face look better
                        ?</h3></div>
                <div class="col-md-6"><h3 class="text-shadow text-md-right text-left">HOW to improve my appearance
                        ?</h3></div>
            </div>
            <div class="row mt-4">
                <p class="w-100 text-center mb-0 font__size__24 font__weight_700">
                    OR
                </p>
                <div class="w-100 after__before__lines two__lines__vertical">
                    <p class="text-center mb-1 font__size__24 font__weight_700">
                        If you search for ways of achieving your PERFECT Face LOOK ...
                    </p>
                    <p class="text-center font-22 font__weight_700 color__666 about__services">
                        Based on anatomical, functional, physiological features of human face we calculate and <span
                            class="two__lines__horizontal">provide you
                    with</span> <br>
                        <span class="text__underline">the BEST result!</span>
                    </p>
                </div>


            </div>
        </div>
        <div class="container mt-4">
            <div class="row justify-content-between">
                @foreach($services as $service)
                    <div class="col-xl col-lg-3 col-6">
                        <a href="{{route('services.service', $service->slug) }}" class="path__to__service">
                            <div class="img-part"><img src="{{ asset('storage/'.$service->image) }}"
                                                       class="img-fluid"/>
                            </div>
                            <div class="text-part"><h4>@lang('services.'.$service->name)</h4></div>
                        </a>

                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- =================== START SERVICE ======================= -->
    <section id="start-service" class="mt-5 vh">
        <div class="container">
            <div class="row justify-content-center">
                <h2 class="text-center start__service__head d-none d-lg-inline-block">No more unnecessary procedure,
                    <br/>
                    no more unsatisfactory result ! <br/>
                    You deserve your Flawless look!
                </h2>
                <div class="start__pic text-center">
                    <img src="{{ asset('storage/img/services/pic.jpg') }}" class="img-fluid d-none d-lg-inline-block"
                         width="80%" alt="">
                    <div class="discover__service">
                        <h1 class="font__weight_700 d-inline-block">
                            <span>Discover procedure/treatment you might need</span>
                            <a href="" class="start__service  ml-md-auto mt-2">start</a>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
