@extends('layouts.reg')

@section('content')
    <!-- FIRST SECTION -->
    <section class="header__options mb-5">
        <!-- =================== NAVIGATION ======================= -->
        <header>
            <div class="container-fluid d-flex d-md-block flex-column">
                @include('layouts.search')
                @include('layouts.navbar')
            </div>
            <div class="container mt-sm-5 mt-3">
                <div class="row pl-md-3  align-items-md-center under__header">
                    <div class="logo col-md-2 col-3">
                        <a href="/">
                            <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="">
                        </a>
                    </div>
                    <div class="heading__text col-md-10 col-9 d-md-block d-flex align-items-end">
                        <h1 class="registration text-center">Registration Form for NEW USER </h1>
                    </div>
                </div>
            </div>
        </header>
        <!-- =================== REGISTER FORM ======================= -->
        <div class="container mt-4">
            <div class="row">
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('auth.register') }}" class="text-center w-100 ac-custom ac-radio ac-fill">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {!! session('status') !!}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif

                    @csrf

                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro {{ $errors->has('full_name') ? ' is-invalid' : '' }}" value="{{ old('full_name') }}" type="text" id="input-7" name="full_name" required/>
                        <label class="input__label input__label--kuro" for="input-7">
                            <span class="input__label-content input__label-content--kuro">Full Name</span>
                        </label>
                    </div>
                    <div class="input input--kuro mb-3">
                        <input class="input__field input__field--kuro" type="text" id="input-8" name="username" required/>
                        <label class="input__label input__label--kuro" for="input-8">
                            <span class="input__label-content input__label-content--kuro">Username</span>
                        </label>
                    </div>


                    <ul class="own__radio d-flex justify-content-between">
                        <!--<li class="w-25"></li>-->
                        <li><input id="r1" name="gender" type="radio" /><label for="r1" class=""><span class="d-block d-md-inline-block">Male</span><img src="{{ asset('storage/img/male.png') }}" alt="" width="60px" class="img-fluid" /></label></li>
                        <li class="r2"><input id="r2" name="gender" type="radio" /><label for="r2" class=""><img src="{{ asset('storage/img/female.png') }}" alt="" width="60px" class="img-fluid" /> <span class="d-block d-md-inline-block">Female</span></label></li>
                    </ul>


                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="date" id="input-9" name="date" required/>
                        <label class="input__label input__label--kuro" for="input-9">
                            <span class="input__label-content input__label-content--kuro">Date Of Birth</span>
                        </label>
                    </div>
                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="email" id="input-10" name="email" required/>
                        <label class="input__label input__label--kuro" for="input-10">
                            <span class="input__label-content input__label-content--kuro">E-mail</span>
                        </label>
                    </div>
                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="password" id="input-11" name="password" required/>
                        <label class="input__label input__label--kuro" for="input-11">
                            <span class="input__label-content input__label-content--kuro">Password</span>
                        </label>
                    </div>
                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="password" id="input-12" name="confirm" required />
                        <label class="input__label input__label--kuro" for="input-12">
                            <span class="input__label-content input__label-content--kuro">Confirm</span>
                        </label>
                    </div>
                    <div class="mb-2">
                        <input class="" id="input-13" type="checkbox"  name="agree" required/>
                        <label class="" for="input-13">I Agree To The  Terms And Conditions </label>
                    </div>
                    <div>
                        <button class="btn reg-btn w-25">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection