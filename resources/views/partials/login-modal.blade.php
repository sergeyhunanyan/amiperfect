<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">LOGIN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="info-message text-center"></p>
                <form action="{{ route('login') }}" data-status="m-login" method="POST"
                      class="text-center w-100 ac-custom ac-radio ac-fill login-modal-form login-form">
                    @csrf
                    {{--<div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="email" id="input-7" name="email" value="{{ old('email') }}" required/>
                        <label class="input__label input__label--kuro" for="input-7">
                            <span class="input__label-content input__label-content--kuro">@lang('generic.email')</span>
                        </label>
                    </div>
                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="password" id="input-11" name="password"
                               required/>
                        <label class="input__label input__label--kuro" for="input-11">
                            <span class="input__label-content input__label-content--kuro">@lang('generic.password')</span>
                        </label>
                    </div>

                    <div class="mb-2">
                        <input class="" id="input-13" type="checkbox" name="remember"/>
                        <label class="" for="input-13">@lang('generic.remembe_me')</label>
                    </div>

                    <button type="submit" class="btn btn reg-btn w-100">@lang('generic.login')</button>--}}
                    <div class="input__rows mt-2">
                        <input type="email" name="email"
                               class="text-center {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('email') }}" placeholder="@lang('generic.email') *" required>
                    </div>
                    <div class="input__rows mt-2">
                        <input type="password" name="password"
                               class="text-center {{ $errors->has('password') ? ' is-invalid' : '' }}"
                               placeholder="@lang('generic.password') *"
                               required>
                    </div>
                    <div class="mt-2 d-flex justify-content-between align-items-center">
                        <button type="submit" class="text-center login__button">Log in</button>
                        <span class="or">or</span>
                        <a href="{{ route('register') }}" data-target="m-login"
                           class="text-center login__button btn-reg">Register</a>
                    </div>
                    <div class="mt-2 d-flex justify-content-between align-items-center">
                        <a href="{{ route('password.request') }}" style="font-size: 13px;">
                            {{ __('generic.forgot') }}
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

