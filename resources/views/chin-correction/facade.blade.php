@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/cropppic-2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css">
    <style>
        .result {
            max-width: 600px;
            max-height: 600px;
        }

        .reset {
            border: 2px solid #f93f3f;
            -webkit-border-radius: 50px;
            -moz-border-radius: 50px;
            border-radius: 50px;
            color: #fff !important;
            font-weight: 600;
            font-size: 17px;
            text-decoration: none;
            -webkit-transition: all .3s ease;
            -moz-transition: all .3s ease;
            -ms-transition: all .3s ease;
            -o-transition: all .3s ease;
            transition: all .3s ease;
            cursor: pointer;
        }
    </style>
@stop

@section('content')
    @include('layouts.navbar')
    <div class="logo col-md-2 col-4">
        <a href="/">
            <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="">
        </a>

    </div>
    <div class="container">
        <div class="row mt-5 align-items-center">
            <div class="navbar__items mr-md-2 mr-1">
                <a href="{{route('service.type', ['service' =>  $service, 'type' => 'side'])}}">Go to Lateral
                    proportion</a>
                @if(isset($test) && $test)
                    <a href="{{route('service.type', [
                    'service' =>  $service,
                    'type' => 'side',
                    'test_id' => $test? 'test-'.$test->id:$test,
                    ])}}">Edit Lateral
                        proportion test {{$test->id}}</a>
                @endif
            </div>
            <h1 class="text-center w-100">POINTS <br> <span class="text-18">Vertical proportions</span></h1>
            <div class="col-md-6 order-2 order-md-1">
                <div class="box d-flex justify-content-between my-2 flex-lg-nowrap flex-wrap">
                    <input type="file" id="file-input"
                           class="btn btn-danger file-load afterCrop own__btn px-3 mr-2 w-100">
                    <div class="order-1 ">
                        <button class="btn btn-danger reset hidden px-3 mr-2 bg-danger">RESET</button>
                    </div>
                    <div class="order-3 order-md-2 w-100 d-flex justify-content-center">
                        <button class="btn btn-success next hidden px-3 own__btn mr-2">NEXT</button>
                        <button class="btn btn-success back hidden px-3 own__btn mr-2">BACK</button>
                    </div>
                    <button class="btn-info save hidden afterCrop own__btn px-3 w-100 mt-md-0 mt-2">CROP</button>
                    <form action="{{route('service.add_data',[
                    'service' => $service,
                    'type' => $type,
                    'test_id' => $test? 'test-'.$test->id:$test,
                    ])}}"
                          class="form-save order-2 order-md-3" method="post">
                        @method('PUT')
                        @csrf
                        <button class="btn btn-success saved hidden own__btn px-3" disabled>SAVE</button>
                    </form>
                </div>
                <div class="result">
                    <img src="{{asset('storage/users/upload_test_default.jpg')}}" width="100%" alt="">
                </div>
            </div>
            <div class="col-md-6 right-text-2 order-1 order-md-2">
                {{--@lang('test.chin_correction.upload_facade')--}}
                <h2 class="chin_right_text text-center">Follow the Instruction</h2>

                <p class="font-22 instruction__p">
                    Face in Relaxed position!
                </p>

                <ol class="instruction__ol">
                    <li>Download your FRONT FACE picture</li>
                    <li>Check the Dots in the Example picture and put the same dots to your picture. (Be careful! If you
                        put it Wrong, result will be NOT CORRECT!)
                    </li>
                    <li>Get the result</li>
                </ol>

                <p class="instruction__p">
                    Now you now all necessary information to make the BEST Aesthetic Solution about your appearence !
                </p>

            </div>
            <div class="col-md-6 right-text order-1 order-md-3"></div>
        </div>
    </div>
@endsection
@section('smoot')@stop
@section('script')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('js/touchui.js')}}"></script>
    <script src="{{asset('js/cr.js')}}"></script>
    <script>

        $('.navbar__menu').attr('style', 'position:relative !important');
        $('.navbar__menu').css('background', '#f2f2f2');
        // vars

        let result = $('.result'),
            img_result = $('.img-result'),
            img_w = $('.img-w'),
            img_h = $('.img-h'),
            options = $('.options'),
            save = $('.save'),
            cropped = $('.cropped'),
            dwn = $('.download'),
            upload = $('#file-input'),
            cropper = '',
            i = 0,
            j = 0;
        // on change show image with crop options

        upload.on('change', (e) => {
            if (e.target.files.length) {
                // start file reader
                const reader = new FileReader();
                reader.onload = (e) => {
                    if (e.target.result) {
                        // create new image
                        img = document.createElement('img');
                        $(img).attr('id', 'image');
                        $(img).attr('src', e.target.result);
                        // clean result before
                        result.html('');
                        // append new image
                        result.append(img);
                        // show save btn and opti	ons
                        save.removeClass('hide');
                        options.removeClass('hide');
                        // init cropper
                        $(img).cropper({
                            aspectRatio: 4 / 4,
                            dragMode: 'move',
                            restore: false,
                            guides: false,
                            dragCrop: false,
                            strict: false,
                            center: false,
                            highlight: false,
                            cropBoxMovable: false,
                            cropBoxResizable: false,
                            toggleDragModeOnDblclick: false,

                        });
                    }
                    $('.save').show()
                };
                reader.readAsDataURL(e.target.files[0]);
            }
        });
        // save on click

        save.on('click', (e) => {
            e.preventDefault();
            // get result to data uri
            let imgSrc = $(img).cropper('getCroppedCanvas').toDataURL('image/jpeg', 0.4);
            lImg = imgSrc;
            // remove hide class of img
            k = '<img src="' + imgSrc + '" width="100%" />';
            // show image cropped
            $(result).html(k);
            $(result).append('<div class="line-vert" ><img src="{{asset('storage/croppic/dot.gif')}}" width="100%"></div>');

            $(".line-vert").draggable({containment: '.result'});
            $('.right-text-2').hide();
            $('.right-text').html('' + '<img src="{{asset('storage/test/chin-correction/step-0.gif')}}" class="step img-fluid" width="">');
            $('.hidden').show();
            $('.afterCrop').hide();
        });
        $('.next').click(function () {
            if (i <= 3) {
                $('.line-vert').eq(i).addClass('stop');
                $('.line-vert').eq(i).clone().appendTo('.result').removeClass('stop').css('left', '10%');
                $('.line-vert').draggable({containment: '.result'});
                $(".stop").draggable("destroy");
                i++
            }
            if (j == 4) {
                $('.saved').attr('disabled', false);
                $('.right-text').html("<h2>Please click Save to continue</h2>");
                return false
            }
            j++;
            $('.right-text').html('' + '<img src="{{asset('storage/test/chin-correction/step-')}}' + j + '.gif" class="step img-fluid" width="">');
        });
        $('.back').click(function () {
            if (i != 0) {
                $('.line-vert').eq(i).remove();
                $('.line-vert').eq(i - 1).removeClass('stop');
                $('.line-vert').draggable({containment: '.result'});
                $(".stop").draggable("destroy");
                i--
            }
            if (j != 0) {
                j--;
                $('.right-text').html('' + '<img src="{{asset('storage/test/chin-correction/step-')}}' + j + '.gif" class="step img-fluid" width="">');
                $('.saved').attr('disabled', true)
            }
        });
        $('.reset').click(function () {
            i = 0;
            j = 0;

            $('.result').html('<img src="{{asset('storage/users/upload_test_default.jpg')}}" width="100%" alt="">')
            $('.right-text').html('');
            $('.right-text-2').show();
            $('.file-load').val('');
            $('.file-load').show();
            $('.hidden').hide();
            $('.saved').attr('disabled', true);
        });
        $('.form-save').submit(function (event) {
            event.preventDefault();
            test_form = $(this);
            data = [];
            params = [];
            $('.line-vert').each(function () {
                t = $(this).css('top');
                h = $(this).css('height');
                data.push(parseInt(t) - parseInt(h))
            });
            data.sort(function (a, b) {
                return a - b
            });
            $('.params').remove();

            $(data).each(function (k) {
                if (k != data.length - 1) {
                    params.push(data[k + 1] - data[k] - (k * 2));
                }
            });
            test_form.append('<input type="hidden" name="data[0][0]" value="' + parseInt(params[0] * 0.264583) + '" class="params">');
            test_form.append('<input type="hidden" name="data[0][1]" value="' + parseInt(params[1] * 0.264583) + '" class="params">');
            test_form.append('<input type="hidden" name="data[0][2]" value="' + parseInt((params[2] + params[3]) * 0.264583) + '" class="params">');
            test_form.append('<input type="hidden" name="data[1][0]" value="' + parseInt(params[2] * 0.264583) + '" class="params">');
            test_form.append('<input type="hidden" name="data[1][1]" value="' + parseInt(params[3] * 0.264583) + '" class="params">');


            test_form.append('<input type="hidden" name="image" value="' + lImg + '" class="params">');
            @guest
            $('#modal-login').modal('show');
            $('.info-message').text('{{__('auth.login_or_register')}}');
            @else
            test_form.unbind("submit").submit();
            @endguest
        });
    </script>
@stop
