@extends('layouts.app')

@section('style')

@stop

@section('content')
    @include('layouts.navbar')

    <!-- =================== SERVICES ======================= -->
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="logo correct__logo col-lg-2 col-4">
                <a href="/">
                    <img src="{{asset('storage/img/logo.png')}}" width="100%" alt="" class="">
                </a>
            </div>
            <div class="col-12 col-lg-6">
                <h1 class="text-center mb-0 mt-3">
                    <span class="d-inline-block  position-relative chin__correction__name">Chin Correction</span>
                </h1>
            </div>
            <div class="col-12 col-lg-4">
                <h1 class="text-center mb-0 mt-3">
                    <span class="d-inline-block  position-relative border-radius-0 bg-transparent chin__correction__name">Important Points</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row align-items-baseline">
            <object data="{{asset('storage/img/important-points/page.svg')}}" width="100%" style='font-family: "Pacifico", cursive'>
                <img src="{{asset('storage/img/important-points/girl.jpg')}}"  />
            </object>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="start__section d-flex align-items-center justify-content-center">
                <div class="text-center px-3 col-6 col-sm order-2 order-sm-1"><a href="" class="own__btn px-4 py-1">Tutorial</a></div>
                <div class="text-center px-3 col-12 col-sm order-1 order-sm-2"><h2>Questionnaire For You</h2></div>
                <div class="text-center px-3 col-6 col-sm order-3 "><a href="{{ route('service.type',['service' => $service, 'type' => 'facade'])}}" class="own__btn px-4 py-1">Start</a></div>
            </div>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row align-items-baseline">
            <object data="{{asset('storage/img/important-points/boy.svg')}}" width="100%" style='font-family: "Pacifico", cursive'>
                <img src="{{asset('storage/img/important-points/boy.jpg')}}"  />
            </object>
        </div>

    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('.animated__line').addClass('on__anime')
            }, 200)
            setTimeout(function () {
                $('.animated__block').addClass('on__anime')
            }, 800)
        })
    </script>
@stop
