@extends('layouts.app')

@section('style')

@stop

@section('content')
    @include('layouts.navbar')

    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <h2 class="text-center">tests list</h2>
                <div id="accordion">
                    @foreach($userinfo as $key => $data)
                        <div class="card">
                            <div class="card-header" id="heading{{$key}}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$key}}"
                                            aria-expanded="true" aria-controls="collapse{{$key}}">
                                        Test ID - {{ $data->id }}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse{{$key}}" class="collapse show" aria-labelledby="heading{{$key}}"
                                 data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="d-flex">
                                                <img src="{{ asset('storage/' . $data->image) }}" width="250"
                                                     height="250" alt="facade">
                                                <ul>
                                                    @if(isset(json_decode($data->details)->facade))
                                                        @foreach(json_decode($data->details)->facade as $i => $item)
                                                            <li>A{{ $i }} - {{$item}}</li>
                                                        @endforeach
                                                    @else
                                                        <p>No resoults</p>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="d-flex">
                                                @if(isset($data->side_image))
                                                    <img src="{{ asset('storage/' . $data->side_image) }}" width="250"
                                                         height="250" alt="side">
                                                @endif
                                                <ul>
                                                    @if(isset(json_decode($data->details)->glsupo))
                                                        @foreach($test_answers as $answer)
                                                            @if($answer->text > json_decode($data->details)->glsupo)
                                                            <li>Glsupo - {{ json_decode($data->details)->glsupo }} ({{$answer->answer}})</li>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    @if(isset(json_decode($data->details)->horizonal))
                                                        <li>Horizonal - {{ json_decode($data->details)->horizonal }}</li>
                                                    @endif
                                                    @if(isset(json_decode($data->details)->labio))
                                                        <li>Labio - {{ json_decode($data->details)->labio }}</li>
                                                    @endif
                                                    @if(isset(json_decode($data->details)->sulapo))
                                                        <li>Sulapo - {{ json_decode($data->details)->sulapo }}</li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@stop