@extends('layouts.app')

@section('content')

    <!-- =================== NAVIGATION ======================= -->
    <header>
        <div class="container-fluid d-flex d-md-block flex-column">

            @include('layouts.search')
            <div class="row justify-content-center my-3">

            </div>
            @include('layouts.navbar')

        </div>
        <div class="container-fluid mt-sm-4 mt-3">
            <div class="row pl-md-3   justify-content-lg-start justify-content-center under__header">
                <div class="logo correct__logo col-lg-2 col-4">
                    <a href="/">
                        <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="" class="">
                    </a>
                </div>
                <div class="col-12 col-lg-5">
                    <h1 class="text-center mb-0 mt-3">
                        <span class="d-inline-block  position-relative chin__correction__name">Chin Correction</span>
                    </h1>
                    <div class="mt-5 pb-4 after__line ">
                        <span class="line__correct"></span>
                        <div class="row w-100 justify-content-around">
                            <div class="col-md-4 col-6 text-part-service-block">
                                <a href="">
                                    <div class="text-part-service">Enter your <br> measurements</div>
                                </a>
                            </div>
                            <div class="col-md-4 col-6 text-part-service-block">
                                {{--<a href="{{Auth::check() ? route('chin.download') : '#0'}}"--}}
                                {{--data-toggle="{{Auth::check() ? '' : 'modal'}}"--}}
                                {{--data-target="{{Auth::check() ? '' : '#modal-login'}}">--}}
                                <a href="{{ route('service.type',['service' => $service, 'type' => 'facade'])}}">
                                    <div class="text-part-service">Download <br> pictures</div>
                                </a>
                            </div>
                            {{--<div class="col-md-3 col-6 mt-3 mt-md-0 text-part-service-block">--}}
                            {{--{{Auth::check() ? route('chin.take-picture'): '#0' }}--}}
                            {{--<a href=""--}}
                            {{--data-toggle="{{Auth::check() ? '' : 'modal'}}"--}}
                            {{--data-target="{{Auth::check() ? '' : '#modal-login'}}">--}}
                            {{--<div class="text-part-service">Take a <br> picture</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            <div class="col-md-4 col-6 mt-3 mt-md-0 text-part-service-block">
                                <a href="{{ route('service.type',['service' => $service, 'type' => 'important']) }}">
                                    <div class="text-part-service">Important <br> points</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-10 mt-lg-0 mt-4 d-flex align-items-center">
                    <div class="w-100 mob__img">
                        <img src="{{ asset('storage/img/chin-correction/1.jpg') }}" class="img-fluid w-100" alt="">
                    </div>
                    <p class="on__image after__line pb-4">
                        <span class="line__correct"></span>
                        The assessment and analysis of the lower facial region is a way to perfect
                        non-surgical <br>
                        aesthetic outcome

                    </p>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6  ">
                    <div>
                        <img src="{{ asset('storage/img/chin-correction/1.jpeg') }}" alt=""
                             class="img-fluid w-100 opacity-5"/></div>
                </div>
                <div class="col-md-6">
                    <p class="on__image__top">
                        The aesthetic appearance of the face is significantly infuenced by the skeleton.An attractive
                        face
                        needs balance and proportion of facial features:
                        <br>
                        the nose, the lips , the lower face and the neck. Correct aesthetic treatment of the chin can
                        impact
                        the appearance of the face. The use of non-surgical dermal filler is growing in popularity with
                        both
                        men and women as a preferable and one of the best treatment option.
                    </p>
                </div>
            </div>
        </div>
    </header>
    <!-- =================== SERVICES ======================= -->
@endsection
