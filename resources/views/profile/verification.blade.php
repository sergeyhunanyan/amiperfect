@extends('layouts.app')


@section('content')
    @include('layouts.navbar')
    <div class="card mx-auto mt-5" style="width: 18rem;">
        <div class="card-body">
            <h6 class="card-subtitle mb-2 text-muted">@lang('auth.not_confirm')</h6>
            <p class="card-text"> @lang('auth.confirm_request',['email' => Auth::user()->email])</p>
            <div class="d-flex justify-content-around">
                <a href="{{route('email.resend-email')}}" class="btn btn-primary">@lang('generic.resend')</a>
                <button type="button" data-toggle="modal" data-target="#change-resend"
                        href="{{route('email.change-resend-email')}}"
                        class="btn btn-primary">@lang('generic.change_email')</button>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="change-resend" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('email.change-resend-email')}}" class="form-validate" method="post">
                    @csrf
                    <div class="modal-body">
                        <label>@lang('generic.email')</label>
                        <input type="email" name="email" value="{{Auth::user()->email}}" class="form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('generic.close')</button>
                        <button type="submit" class="btn btn-primary">@lang('generic.change')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop