@if($user_info->count()>0)

    <h2 class="text-center mt-5">@lang('generic.test') {{__('services.'.$service->name)}}</h2>
    <div id="accordion" class="mt-5">
        @foreach($user_info->sortByDesc('id') as $key => $data)
            <div class="card">
                <div class="card-header collapse-header" id="heading-{{$data->id}}">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse"
                                data-target="#collapse{{$service->slug.'-'.$key}}"
                                aria-expanded="true" aria-controls="collapse{{$service->slug.'-'.$key}}">
                            <span>Test {{ $data->id }}</span> <br>
                            <span>Created : {{$data->created_at->diffForHumans()}}</span> <br>
                            @if($data->updated_at->gt($data->created_at))
                                <span>Updated : {{$data->updated_at->diffForHumans()}}</span>
                            @endif
                        </button>
                    </h5>
                </div>

                <div id="collapse{{$service->slug.'-'.$key}}"
                     class="collapse
                     @if($test_id) @if($test_id == $data->id)  show @endif @elseif($data->id == $user_info->max('id')) show @endif"
                     aria-labelledby="heading{{$service->slug.'-'.$key}}"
                     data-parent="#accordion">
                    <div class="card-body">
                        <div class="row justify-content-xl-between justify-content-center">
                            @php($facade = json_decode($data->facade, true))
                            @php($template = json_decode($service->options, true))
                            @php($answer =  $service->answers->where('gender', Auth::user()->gender))

                            <div class="col-xl-6 ">
                                <div class="navbar__items d-flex justify-content-start mb-2">
                                    <a href="{{route('service.type', [
                                    'service' =>  $service->slug,
                                    'type' => 'facade',
                                    'test_id' => 'test-'.$data->id
                                    ])}}">Go to facade proportion</a>
                                </div>


                                @if($facade)

                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <img src="{{ $facade['image']}}" width="100%" alt="facade">
                                        </div>

                                        <ul class="col-6 for-mob-font">
                                            @if(isset($facade['details']))
                                                @foreach($template[0]['facade']['option'] as $i => $option)
                                                    {{--{{dd($option)}}--}}

                                                    <?php unset($option['type'])?>
                                                    @foreach($option as $key => $val)
                                                        {{--{{dd($facade['details'][$i])}}--}}
                                                        <li>{{ $val . ' : ' . $facade['details'][$i][$key]}} (MM)</li>
                                                    @endforeach
                                                    <?php
                                                    $equal = 'equal';
                                                    foreach ($facade['details'][$i] as $key => $val) {
                                                        if (isset($facade['details'][$i][$key - 1]) && $facade['details'][$i][$key - 1] != $val) {

                                                            $equal = 'not_equal';
                                                            break;
                                                        }
                                                    }



                                                    ?>
                                                    @if(isset($answer->first()->details))
                                                        Answer
                                                        : {{json_decode($answer->first()->details, true)['facade']['option'][$i][$equal]}}

                                                    @endif
                                                    <hr>
                                                @endforeach
                                            @else
                                                <p>No resoults</p>
                                            @endif
                                        </ul>
                                    </div>

                                @endif
                            </div>

                            @php($side = json_decode($data->side, true))

                            <div class="col-xl-6 mt-xl-0 mt-2">
                                <div class="navbar__items d-flex justify-content-end mb-2">
                                    <a href="{{route('service.type', [
                                    'service' =>  $service->slug,
                                    'type' => 'side',
                                    'test_id' => 'test-'.$data->id
                                    ])}}">Go
                                        to Lateral proportion</a>
                                </div>
                                @if($side)
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <img src="{{ $side['image'] }}"
                                                 width="100%"
                                                 height="" alt="side">
                                        </div>

                                        <ul class="col-6 for-mob-font">
                                            @foreach($template[0]['side']['option'] as $i => $option)
                                                <?php unset($option['type'])?>
                                                @foreach($option as $key => $val)
                                                    {{--{{dd($facade['details'][$i])}}--}}
                                                    <li>
                                                        {{ $val . ' : ' . $side['details'][$i][$key]}}   {{($service->slug == 'nose-correction' && $key == 6) || ($service->slug == 'nose-correction' && $key == 7) ? '' : '(DEG)'}}

                                                        <?php
                                                        $angle = $side['details'][$i][$key];
                                                        $details = isset($answer->first()->details) ? json_decode($answer->first()->details, true)['side']['option'][$i][$key] : false;
                                                        if ($details):
                                                            $size = explode('-', $details['size']);
                                                            $result = '';
                                                            if (isset($size[1])) {
                                                                if ($angle > $size[0] && $angle < $size[1]) {
                                                                    $result = $details['standard'];
                                                                } elseif ($angle == $size[0]) {
                                                                    $result = $details['standard'];
                                                                } elseif ($angle < $size[0]) {
                                                                    $result = $details['small'];
                                                                } elseif ($angle > $size[0]) {
                                                                    $result = $details['big'];
                                                                }
                                                            } elseif ($angle == $size[0]) {
                                                                $result = $details['standard'];
                                                            } elseif ($angle < $size[0]) {
                                                                $result = $details['small'];
                                                            } elseif ($angle > $size[0]) {
                                                                $result = $details['big'];
                                                            }
                                                            if($result){
                                                                echo '<br>Answer : ' . $result . '<hr>';
                                                            }

                                                        endif;
                                                        ?>

                                                    </li>




                                                @endforeach
                                            @endforeach









                                            {{--@if(isset($side['details']))--}}
                                            {{--@foreach($side->details as $name => $item)--}}
                                            {{--<li>{{$name.' : '. $item}} (DEG)</li>--}}
                                            {{--@endforeach--}}

                                            {{--@endif--}}

                                            {{--@if(isset(json_decode($data->details)->glsupo))--}}
                                            {{--@foreach($test_answers as $answer)--}}
                                            {{--@if($answer->text > json_decode($data->details)->glsupo)--}}
                                            {{--<li>Glsupo--}}
                                            {{--- {{ json_decode($data->details)->glsupo }}--}}
                                            {{--({{$answer->answer}})--}}
                                            {{--</li>--}}
                                            {{--@endif--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--@if(isset(json_decode($data->details)->horizonal))--}}
                                            {{--<li>Horizonal--}}
                                            {{--- {{ json_decode($data->details)->horizonal }}</li>--}}
                                            {{--@endif--}}
                                            {{--@if(isset(json_decode($data->details)->labio))--}}
                                            {{--<li>Labio - {{ json_decode($data->details)->labio }}</li>--}}
                                            {{--@endif--}}
                                            {{--@if(isset(json_decode($data->details)->sulapo))--}}
                                            {{--<li>Sulapo - {{ json_decode($data->details)->sulapo }}</li>--}}
                                            {{--@endif--}}
                                        </ul>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@else
    <h2 class="text-center mt-5">@lang('generic.not_test')</h2>

@endif




