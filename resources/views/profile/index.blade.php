@extends('layouts.app')
@section('content')
    @include('layouts.navbar')
    <div class="logo col-md-2 col-4">
        <a href="/">
            <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="">
        </a>

    </div>
    <div class="container mt-5">
        <nav>
            <div class="nav nav-tabs " id="nav-tab" role="tablist">
                @foreach($services as $item)
                    <a class="nav-item nav-link {{$service->slug == $item->slug ? 'active' : '' }}"
                       id="jaw-line-correction-tab" href="{{route('profile.service',$item->slug)}}"
                       role="tab"
                       aria-controls="nav-jaw-line-correction"
                       aria-selected="true">@lang('services.'.$item->name)
                    </a>
                @endforeach
            </div>
        </nav>
    </div>
    <div class="container-fluid ">
        <div class="tab-content" id="nav-tabContent">
            @foreach($services as $item)
                @if($service->id == $item->id)
                    <div class="tab-pane fade {{$service->slug == $item->slug ? 'show active' : '' }}"
                         id="jaw-line-correction"
                         role="tabpanel"
                         aria-labelledby="jaw-line-correction-tab">
                        @include('profile.test-result')
                    </div>
                @endif
            @endforeach
        </div>
    </div>



@stop
@section('script')
    <script>
        $(document).ready(function () {
            @if($test_id)
                offTop =  $('#heading-{{$test_id}}').offset().top;
            $('html,body').animate({
                scrollTop : offTop,
            })
            @endif
        })

    </script>
@stop
