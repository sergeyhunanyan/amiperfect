@extends('layouts.app')

@section('content')


    <!-- FIRST SECTION -->
    <section class="header__options mb-5">
        <!-- =================== NAVIGATION ======================= -->
        <header>
            <div class="container-fluid d-flex d-md-block flex-column">
                @include('layouts.search')
                <div class="row pl-md-3  align-items-md-center under__header">
                    <div class="logo col-md-2 col-3">
                        <a href="/"><img src="{{ asset('storage/img/logo.png') }}" width="100%" alt=""></a>
                    </div>
                    <div class="heading__text col-md-10 col-9 d-md-block d-flex align-items-end">
                        <h1 class="">is available for you to measure your face pefaction ! </h1>
                    </div>
                </div>
                <div class="row justify-content-end order-3">
                    <div
                        class="col-lg-11 d-flex justify-content-md-between justify-content-center flex-column flex-md-row">
                        @include('layouts.navbar')
                        <div class="center__text">
                            <h2>It`s perfect Time to be Flawless... </h2>
                            <h3 class="d-none d-md-block">and it`s Easy! Just check</h3>
                        </div>
                        @if(Auth::guest())
                            <form method="POST" data-status="home-login" action="{{ route('login') }}" aria-label="{{ __('generic.login') }}"
                                  class="login__form login-form">
                                {{ csrf_field() }}
                                <div class="input__rows mt-2">
                                    <input type="email" name="email"
                                           class="text-center {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           value="{{ old('email') }}" placeholder="@lang('generic.email') *" required>
                                </div>
                                <div class="input__rows mt-2">
                                    <input type="password" name="password"
                                           class="text-center {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           placeholder="@lang('generic.password') *"
                                           required>
                                </div>
                                <div class="mt-2 d-flex justify-content-between align-items-center">
                                    <button type="submit" class="text-center login__button">Log in</button>
                                    <span class="or">or</span>
                                    <a href="{{ route('register') }}" data-target="home-login" class="text-center login__button btn-reg">Register</a>
                                </div>
                                <div class="mt-2 d-flex justify-content-between align-items-center">
                                    <a href="{{ route('password.request') }}" style="font-size: 13px;">
                                        {{ __('generic.forgot') }}
                                    </a>
                                </div>
                            </form>
                        @else
                            <div class="login__form">
                                <img src="{{asset('storage/'.Auth::user()->avatar)}}" alt="">
                                <h4 class="text-center">{{ Auth::user()->user_name }}</h4>
                                <div class="mt-2 d-flex justify-content-between align-items-center">
                                    <a href="{{route('profile.index')}}"
                                       class="text-center login__button mr-3">Profile</a>
                                    <a class="text-center login__button" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </header>
        <!-- =================== OPTIONS ======================= -->
        <div id="section-options">
            <div class="container-fluid">
                <h2 class="options-header text-center w-100">OPTIONS</h2>
                <div class="row justify-content-between  ">
                    <div class="left__side__img col-md-4 col-6 order-1 d-flex align-content-end">
                        <div class="w-100 d-flex ">
                            <img src="{{ asset('storage/img/mo.jpg') }}" alt="" class="img-fluid align-self-end"
                                 width="100%"/>
                        </div>
                    </div>

                    <div class="d-md-none d-flex col-6 order-2 justify-content-between mob__aline">
                        <nav class="navbar__menu">
                            <ul class="">
                                <li class="navbar__items__left ml-2"><a href="">Malocclusion</a></li>
                                <li class="navbar__items__left ml-2"><a href="">Treatments</a></li>
                                <li class="navbar__items__left ml-2"><a href="">Doctors</a></li>
                            </ul>
                        </nav>
                        <div>
                            <img src="{{ asset('storage/img/me.png') }}" class="img-fluid" alt=""/>
                        </div>
                    </div>


                    <div
                        class=" mob__mt__2 bit__correction w-100 position-absolute order-xl-2 order-3 justify-content-center d-flex align-self-center">
                        <div class="col-xl-8 col-12">
                            <div class="bg__blue ">
                                <div class="bg__blue__inner py-2">
                                    <h2 class="text-center text-capitalize pb-0">bite correction</h2>
                                    <p class="mb-0">
                                        Enter your JAWS and DENTAL measurement and check your dental
                                        occlusion.
                                    </p>
                                    <p>
                                        Find how your bite affects your look and how impact your APPEARANCE!
                                    </p>

                                    <div class="d-flex justify-content-center">

                                        <div>
                                            <h4 class="text-left">
                                                Check WAYS to Improve it:
                                            </h4>
                                            <div>
                                                <p class="mb-2 text-center">Recomendations,</p>
                                                <p class="mb-2 text-center">Treatmens, </p>
                                                <p class="mb-2 text-center">Doctors</p>
                                            </div>

                                        </div>
                                        <div>
                                            <img src="{{ asset('storage/img/me.png') }}" alt=""
                                                 class="d-none d-md-block" width="100%"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav class="navbar__menu d-none d-md-block">
                                <ul class="d-flex justify-content-center">
                                    <li class="navbar__items__left ml-2"><a href="">Malocclusion</a></li>
                                    <li class="navbar__items__left ml-2"><a href="">Treatments</a></li>
                                    <li class="navbar__items__left ml-2"><a href="">Doctors</a></li>
                                </ul>
                            </nav>
                            <div class="col-12 mt-3">
                                <div class="text-center mt-2">
                                    <a href="" class="btn own__btn">Start-now <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="right__side__img col-md-5 order-xl-3 order-2 d-md-block d-none">
                        <div class="w-100 d-flex ">
                            <img src="{{ asset('storage/img/men.jpg') }}" alt="" class="img-fluid align-self-center"
                                 width="100%"/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- SECOND SECTION -->
    <section class="mt-5 second__section">
        <div class="container-fluid ">
            <div class="row mob__aline">
                <div class="col-md-5  col-12 order-2 order-md-1  align-self-md-center align-self-end mob__aline">
                    <div><img src="{{ asset('storage/img/girl.jpg') }}" width="100%" alt=""
                              class="img-fluid rounded-md-circle"></div>
                </div>
                <div class="col-md-4 order-2 order-md-2 offset-md-3 align-self-center d-md-block d-none">
                    <div>
                        <img src="{{ asset('storage/img/beauty.jpg') }}" alt="" class="img-fluid rounded-60"/>
                    </div>
                </div>
                <div
                    class="col-md-12 col-12 order-1 order-md-3 bit__correction bit__correction__top w-100 position-absolute justify-content-center d-flex align-self-end pb-3">
                    <div class="col-md-9 mr-md-5">
                        <h2 class="text-center text-capitalize pb-0 mob__font">Non-surgical face treatment</h2>
                        <div class="bg__blue mob__bg__none">
                            <div class="bg__blue__inner py-2">

                                <div class="mb-0 text-center d-flex font-22">
                                    Minimally invisive procedures in the form of injections. <br class="mob__none">
                                    If you are curious about your face proportions or/and symethry. <br
                                        class="mob__none">
                                    Want to improve your looks or return blooming youth. <br class="mob__none">
                                    Need information about new ways of treatment. <br class="mob__none">
                                    Check this option!
                                    <img src="{{ asset('storage/img/5.png') }}" alt=""
                                         class="img-fluid align-self-end mob__none"/>
                                </div>
                            </div>
                        </div>
                        <nav class="navbar__menu">
                            <ul class="d-flex justify-content-center">
                                <li class="navbar__items__left ml-2"><a href="">Filler injections</a></li>
                                <li class="navbar__items__left ml-2"><a href="">Doctors</a></li>
                                <li class="navbar__items__left ml-2"><a href="">Science <span class="mob__none">behind treatment</span>
                                    </a></li>
                                <li class="navbar__items__left ml-2 mob__none"><a href="" class="btn own__btn">START <i
                                            class="fas fa-angle-right"></i></a></li>

                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- THIRD SECTION -->
    <section id="child">
        <div class="container-fluid">
            <div class="row">
                <div class="w-100 bg__child d-flex  flex-wrap">
                    <div class="col-12 flex-auto">
                        <div
                            class="col-lg-6 d-flex flex-md-column flex-md-nowrap flex-wrap offset-lg-6 justify-content-between">
                            <h2 class="icon_column__name text-md-right text-center">How it WORKS</h2>
                            <div class="d-md-none mx__-30">
                                <img src="{{ asset('storage/img/child.jpg') }}" class="img-fluid" alt="">
                            </div>


                            <div
                                class="mx__-30 d-flex align-items-center mb-2 col-md-12 col-6  justify-content-md-end justify-content-center flex-md-nowrap flex-wrap">
                                <div class="icon__part col-lg-4 col-md-6 col-12 text-md-right text-center">
                                    <img src="{{ asset('storage/img/6.png') }}" alt="" class="img-fluid "/>
                                </div>
                                <div class="text__part text-center col-lg-8 col-md-6 col-12 py-3">
                                    <p class="font__weight_600 font__size__18 mb-1">Enter your measurments Determine
                                        your
                                        face attractiveness
                                        Find procedure /treatment right for you!</p>
                                </div>
                            </div>

                            <div
                                class="mx__-30 d-flex align-items-center mb-2 col-md-12 col-6 justify-content-md-end justify-content-center flex-md-nowrap flex-wrap">
                                <div class="icon__part col-lg-4 col-md-6 col-12 text-md-right text-center">
                                    <img src="{{ asset('storage/img/4.png') }}" alt="" class="img-fluid "/>
                                </div>
                                <div class="text__part text-center col-lg-8 col-md-6 col-12 py-3">
                                    <p class="font__weight_600 font__size__18 mb-1">Find professional sutable for
                                        you </p>
                                    <p class="font__weight_600 font__size__18 mb-1">Choose the Best Deal !</p>
                                </div>
                            </div>
                            <h3 class="d-block d-md-none col-12 text-center my-3 mob__font__family min__mob">TIME to be
                                Flawless !</h3>
                            <div
                                class="mx__-30 d-flex align-items-center mb-2 col-md-12 col-6 justify-content-md-end justify-content-center flex-md-nowrap flex-wrap">
                                <div class="icon__part col-lg-4 col-md-6 col-12 text-md-right text-center">
                                    <img src="{{ asset('storage/img/3.png') }}" alt="" class="img-fluid "/>
                                </div>
                                <div class="text__part text-center col-lg-8 col-md-6 col-12 py-3">
                                    <p class="font__weight_600 font__size__18 mb-1">JUST in TWO clicks !</p>
                                    <p class="font__weight_600 mb-0 text-mb-left text-center">Book an appoitment or</p>
                                    <p class="font__weight_600 mb-0 text-mb-left text-center">schedule a consultation
                                        with
                                        Professional! </p>
                                </div>
                            </div>

                            <div
                                class="mx__-30 d-flex align-items-center mb-2 col-md-12 col-6 justify-content-md-end justify-content-center flex-md-nowrap flex-wrap">
                                <div class="icon__part col-lg-4 col-md-6 col-12 text-md-right text-center ">
                                    <img src="{{ asset('storage/img/1.png') }}" alt="" class="img-fluid "/>
                                </div>
                                <div class="text__part text-center col-lg-8 col-md-6 col-12 py-3">
                                    <p class="font__weight_600 font__size__18 mb-1 font__size__22 text-underline">Enjoy
                                        your
                                        perfect result</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 p-0 flex-auto align-self-end d-flex flex-wrap">
                        <div
                            class="heading__text heading__text__two d-md-flex d-none align-items-center justify-content-around w-100">
                            <h2 class="mb-0">TIME to be Flawless ! </h2>
                            <div class="z__index">
                                <a href="" class="btn own__btn bg__white">Start now <i
                                        class="fas fa-angle-right"></i></a>
                            </div>

                        </div>
                        <div
                            class="welcome d-flex align-items-center justify-content-around py-3 w-100 flex-auto flex-md-nowrap flex-wrap">
                            <h4 class="mb-md-0 mb-2 ">Want more information?</h4>
                            <form action="" class="d-flex align-items-center justify-content-around">
                                <div class="input-group">
                                    <input type="text" aria-label="First name" placeholder="Name" class="form-control">
                                    <input type="email" aria-label="Last name" placeholder="E-mail"
                                           class="form-control">
                                    <input type="text" aria-label="Question" placeholder="Question"
                                           class="form-control mob__question">
                                    <input type="submit" aria-label="Submit" value="Send"
                                           class="form-control own__btn mob__own__btn ">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
