@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/cropppic-2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css">
    <style>
        .fabric_custom {
            position: absolute;
            top: 0;
            width: 100% !important;
            height: 100% !important;
            margin-left: -15px !important;
            margin-right: -15px !important;
            display: none;
        }

        .canvas-container {
            width: 100% !important;
            height: 100% !important;
            overflow: hidden;
        }

        .text {
            font-weight: bold;
            font-size: 30px;
        }

        .result {
            max-width: 600px;
            max-height: 600px;
        }
        .reset{
            border: 2px solid #f93f3f;
            -webkit-border-radius: 50px;
            -moz-border-radius: 50px;
            border-radius: 50px;
            color: #fff !important;
            font-weight: 600;
            font-size: 17px;
            text-decoration: none;
            -webkit-transition: all .3s ease;
            -moz-transition: all .3s ease;
            -ms-transition: all .3s ease;
            -o-transition: all .3s ease;
            transition: all .3s ease;
            cursor: pointer;
        }
    </style>
@stop

@section('content')
    @include('layouts.navbar')
    <div class="logo col-md-2 col-4">
        <a href="/">
            <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="">
        </a>

    </div>
    <div class="container">
        <div class="row mt-5 align-items-center">
            <div class="navbar__items mr-md-2 mr-1 mb-3 w-100 text-center">
                <a href="{{route('service.type', ['service' =>  $service, 'type' => 'facade'])}}" class="mr-2">Go to Front
                    proportion</a>
                @if(isset($test) && $test)
                    <a href="{{route('service.type', [
                    'service' =>  $service,
                    'type' => 'side',
                    'test_id' => $test? 'test-'.$test->id:$test,
                    ])}}">Edit Front
                        proportion test {{$test->id}}</a>
                @endif
            </div>
            <h1 class="text-center w-100">Lateral proportion</h1>
            <div class="col-12 order-2 order-md-1">
                <div class="box d-flex justify-content-between my-2 col-md-6 flex-lg-nowrap flex-wrap">
                    <input type="file" id="file-input" class="btn btn-danger file-load afterCrop own__btn px-3 mr-2 w-100">
                    <div class="order-1 ">
                        <button class="btn btn-danger reset hidden px-3 mr-2 bg-danger">RESET</button>
                    </div>
                    <div class="order-3 order-md-2 w-100 d-flex justify-content-center">
                        <button class="btn btn-success next hidden px-3 own__btn mr-2" disabled>NEXT</button>
                        <button class="btn btn-success back hidden px-3 own__btn mr-2">BACK</button>
                    </div>
                    <button class="btn-info save hidden afterCrop own__btn px-3 w-100 mt-md-0 mt-2">CROP</button>
                    <form action="{{route('service.add_data',[
                    'service' => $service,
                    'type' => $type,
                    'test_id' => $test? 'test-'.$test->id:$test,
                     ])}}"
                          class="form-save order-2 order-md-3" method="post">
                        @method('PUT')
                        @csrf
                        <button class="btn btn-success saved hidden own__btn px-3" disabled>SAVE</button>
                    </form>
                </div>
            </div>
            <div class="col-md-6 order-2 order-md-1">


                <div class="result">
                    <img src="{{asset('storage/img/lip-injection/upload_test_side.png')}}" width="100%" alt="">
                </div>
                <div class="fabric_custom" style="width: 500px;height: 500px;margin: auto;">
                    <canvas width="550" height="550" id="c" style=""></canvas>
                </div>

            </div>

            <div class="col-md-6 right-text-2 order-1 order-md-2">
                {{--@lang('test.chin_correction.upload_facade')--}}
                <h2 class="chin_right_text text-center">Follow the Instruction</h2>
                <ol class="instruction__ol">
                    <li>Download your PROFILE (side view)</li>
                    <li>Check The Spots o in The Example picture and put the same spots to your picture. (Be very
                        careful ! Dots in wrong place give you WRONG result!)
                    </li>
                    <li>Get the result</li>
                </ol>

                <p class="instruction__p text-center">
                    You are on the right track to your <br>
                    Flawless Look!

                </p>

                <b class="text"></b>

            </div>
            <div class="col-md-6 right-text mt-2 mt-md-0 order-1 order-md-3">
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Croppic Scripts -->
    <script src="{{asset('js/cr.js')}}"></script>
    <!-- CANVAS Scripts -->
    <script src="{{ asset('js/fabric.js') }}"></script>
    <script>
        $('.navbar__menu').attr('style', 'position:relative !important');
        $('.navbar__menu').css('background', '#f2f2f2');


        (function () {

            var canvas = this.__canvas = new fabric.Canvas('c', {selection: false,});

            fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';

            function makeCircle(left, top, line1, line2) {
                c = new fabric.Circle({
                    left: left,
                    top: top,
                    strokeWidth: 3,
                    radius: 8,
                    fill: 'yellow',
                    stroke: 'red',
                    originX: 'center',
                    originY: 'center'
                });
                c.hasControls = c.hasBorders = false;

                c.line1 = line1;
                c.line2 = line2;
                return c;
            }


            function makeLine(coords) {
                return new fabric.Line(coords, {
                    fill: 'red',
                    stroke: 'red',
                    strokeWidth: 2,
                    selectable: false
                });
            }

            var line1 = makeLine([100, 75, 350, 75]),
                line2 = makeLine([350, 75, 350, 250]);

            canvas.add(line1, line2);
            canvas.add(
                makeCircle(line1.get('x1'), line1.get('y1'), null, line1),
                makeCircle(line1.get('x2'), line1.get('y2'), line1, line2),
                makeCircle(line2.get('x2'), line2.get('y2'), line2)
            );


            canvas.on('object:moving', function (e) {
                resWidth = $('.result > img').width();
                resHeight = $('.result > img').height();
                var p = e.target;

                if (p.left >= resWidth) {
                    p.canvas._activeObject.left = resWidth;
                    p.left = resWidth;
                } else if (p.left <= 0) {
                    p.canvas._activeObject.left = 5;
                    p.left = 5;
                }

                if (p.top >= resHeight) {
                    p.canvas._activeObject.top = resHeight - 5;
                    p.top = resHeight - 5;
                } else if (p.top <= 0) {
                    p.canvas._activeObject.top = 5;
                    p.top = 5;
                }
//                if(p.top  >= resHeight)return;
//                if(p.right  >= resAll-30)return;
                // console.log(p);


                p.line2 && p.line2.set({'x1': p.left, 'y1': p.top});
                p.line1 && p.line1.set({'x2': p.left, 'y2': p.top});

                canvas.renderAll();

                a1 = line1.get('x1');
                a2 = line1.get('y1');

                c1 = line2.get('x2');
                c2 = line2.get('y2');


                b1 = line2.get('x1');
                b2 = line2.get('y1');

                AB = Math.sqrt(Math.pow((b1 - a1), 2) + Math.pow((b2 - a2), 2));
                BC = Math.sqrt(Math.pow((b1 - c1), 2) + Math.pow((b2 - c2), 2));
                AC = Math.sqrt(Math.pow((c1 - a1), 2) + Math.pow((c2 - a2), 2));

//                if(line1.x1 >= resAll){
//
//                }
                if (j <= 1) {
                    $('.next').attr('disabled', false)
                    resultDegree = Math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB)) * 180 / Math.PI;
                    resultDegree = Math.round(resultDegree)

                }
            });

        })();
        // vars
        let result = $('.result'),
            img_result = $('.img-result'),
            img_w = $('.img-w'),
            img_h = $('.img-h'),
            options = $('.options'),
            save = $('.save'),
            cropped = $('.cropped'),
            dwn = $('.download'),
            upload = $('#file-input'),
            cropper = '',
            j = 0;
        // on change show image with crop options
        upload.on('change', (e) => {
            if (e.target.files.length) {
                // start file reader
                const reader = new FileReader();
                reader.onload = (e) => {
                    if (e.target.result) {
                        // create new image
                        img = document.createElement('img');
                        $(img).attr('id', 'image');
                        $(img).attr('src', e.target.result);
                        // clean result before
                        result.html('');
                        // append new image
                        result.append(img);
                        // show save btn and opti	ons
                        save.removeClass('hide');
                        options.removeClass('hide');
                        // init cropper
                        $(img).cropper({
                            aspectRatio: 4/4,
                            dragMode: 'move',
                            restore: false,
                            guides: false,
                            center: false,
                            highlight: false,
                            cropBoxMovable: false,
                            cropBoxResizable: false,
                            toggleDragModeOnDblclick: false,

                        });
                    }
                    $('.save').show()
                };
                reader.readAsDataURL(e.target.files[0]);
            }
        });
        // save on click
        save.on('click', (e) => {

            e.preventDefault();
            // get result to data uri
            let imgSrc = $(img).cropper('getCroppedCanvas').toDataURL('image/jpeg',0.4);
            lImg = imgSrc;
            // remove hide class of img
            k = '<img src="' + imgSrc + '" width="100%" />';
            // show image cropped
            $(result).html(k);


            $('.right-text-2').hide();
            $('.right-text').html('' + '<img src="{{asset('storage/test/lip-injection/side/step-0.gif')}}" class="step img-fluid" width="">');
            $('.hidden').show();
            $('.afterCrop').hide();
            $('.fabric_custom').show()
        });
        data = [];
        m = 0;
        $('.next').click(function () {

            if (m != 2) {
                data.push(resultDegree);
                m++;
            }
            if (j >= 1) {
                $('.saved').attr('disabled', false);
                $('.right-text').html("<h2>Please click Save to continue</h2>");
                j = 5;
                return false
            }
            $('.next').attr('disabled', true);
            j++;
            $('.right-text').html('' + '<img src="{{asset('storage/test/lip-injection/side/step-')}}' + j + '.gif" class="step img-fluid" width="">');
        });
        $('.back').click(function () {
            if (m != 0) {
                data.pop();
                m--
            }

            if (j != 0) {
                j--;
                $('.right-text').html('' + '<img src="{{asset('storage/test/lip-injection/side/step-')}}' + j + '.gif" class="step img-fluid" width="">');
                $('.saved').attr('disabled', true)
            }
        });
        $('.reset').click(function () {
            j = 0;

            $('.result').html('<img src="{{asset('storage/img/lip-injection/upload_test_side.png')}}" width="100%" alt="">')
            $('.right-text').html('');
            $('.right-text-2').show();
            $('.file-load').val('');
            $('.file-load').show();
            $('.hidden').hide();
            $('.saved').attr('disabled', true);
            $('.fabric_custom').hide()
        });
        $('.form-save').submit(function (event) {
            event.preventDefault();
            test_form = $(this);


            $(data).each(function (k) {
                test_form.append('<input type="hidden" name="data[0][]" value="' + data[k] + '" class="params">');
            });
            test_form.append('<input type="hidden" name="image" value="' + lImg + '" class="params">');
            @guest
            $('#modal-login').modal('show');
            $('.info-message').text('{{__('auth.login_or_register')}}');
            @else
            test_form.unbind("submit").submit();
            @endguest

        });


    </script>

@stop
