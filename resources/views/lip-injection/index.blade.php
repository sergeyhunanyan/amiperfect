@extends('layouts.app')

@section('style')

@stop

@section('content')
    @include('layouts.navbar')

    <!-- =================== SERVICES ======================= -->
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="logo correct__logo col-lg-2 col-4">
                <a href="/">
                    <img src="{{asset('storage/img/logo.png')}}" width="100%" alt="" class="">
                </a>
            </div>
            <div class="col-12 col-lg-10">
                <h1 class="text-center mb-0 mt-3">
                    <span class="d-inline-block  position-relative chin__correction__name">Lip injection</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row ">
            <div class="col-lg-8 order-lg-1 order-2">
                <img src="{{asset('storage/img/lip-injection/girl2.jpg')}}" alt="" class="img-fluid">
            </div>
            <div class="col-lg-4 text-center order-lg-2 order-1">
                <img src="{{asset('storage/img/lip-injection/beauty.jpg')}}" alt="" class="img-fluid">

                <p class="text-center font__size__18 ">
                    <b>
                        Procedure of Lip Enhancement is one of the most frequently requested cosmetic procedures. But to get the most aesthetic lips, professionals must combine
                    </b>


                </p>
                <b>
                    Science and Art!
                </b>
            </div>
        </div>
        <div class="row mt-1">
            <p class="font-22 px-3 bold">
                <b>
                    The purpose of lip injection is to improve and balance the appearance of the lips with other facial features. The process begins by applying the basic artistic principle! The Professional who perform it , is like artist with suringe instead of brush...
                </b>

            </p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="start__section d-flex align-items-center justify-content-center">
                <div class="text-center px-3 col-6 col-sm order-2 order-sm-1"><a href="" class="own__btn px-4 py-1">Tutorial</a></div>
                <div class="text-center px-3 col-12 col-sm order-1 order-sm-2"><h2>Questionnaire For You</h2></div>
                <div class="text-center px-3 col-6 col-sm order-3 ">
                    <a href="{{ route('service.type',['service' => $service, 'type' => 'facade'])}}"
                       class="own__btn px-4 py-1">Start</a>
                </div>
            </div>
        </div>

    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <object data="{{asset('storage/img/lip-injection/second.svg')}}" width="100%" style='font-family: "Pacifico", cursive'>
                    <img src="{{asset('storage/img/lip-injection/second.png')}}"  />
                </object>
            </div>
        </div>
    </div>
@endsection

@section('script')


@stop
