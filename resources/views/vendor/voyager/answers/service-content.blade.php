
@if(!isset($details))
    <p>Not a results!</p>
@else
    @if(isset($details['facade']['option']))
        <div class="form-group  col-md-12">
            <h2>Facade</h2>
        </div>
        @foreach($details['facade']['option'] as $i=>$item)
            @if ($item['type'] == 'compare')
                @php
                    $string = '';
                unset($item['type']);
                foreach ($item as $key => $value){
                    $string .= ($key==0 ? '' : ' and ').$value;
                }
                $col = count($details['facade']['option'])
                @endphp

                <div class="form-group text-center {{$col == 1 ? 'col-md-12' : 'col-md-6'}}">
                    <label for="name">{!! $string !!}</label><br>
                    <div class="col-md-5">
                        <input type='text' class="form-control" name='details[facade][option][{{$i}}][equal]'
                               value="{{$params['facade']['option'][$i]['equal']?? ''}}"
                               placeholder="Is equal"/>
                    </div>
                    <div class="col-md-1">
                        <label for="name"> OR </label>
                    </div>
                    <div class="col-md-5">
                        <input type='text' class="form-control"
                               name='details[facade][option][{{$i}}][not_equal]'
                               value="{{$params['facade']['option'][$i]['not_equal']?? ''}}"
                               placeholder="Is not equal"/>
                    </div>

                </div>
            @endif
        @endforeach
    @endif
    @if(isset($details['side']['option']))
        <div class="form-group  col-md-12">
            <hr style="border-top: 1px solid #e4eaec">
            <h2>Side</h2>
        </div>
        @foreach($details['side']['option'] as $i=>$item)
            @if ($item['type'] == 'compare')
                @php
                    $string = '';
                unset($item['type']);

                @endphp
                @foreach ($item as $key => $value)
                    <div class="form-group  col-md-12 text-center">
                        <label for="name">{{$value}}</label><br>
                        <div class="col-md-3">
                            <div class="col-md-12">
                                <input type='text' class="form-control"
                                       name='details[side][option][{{$i}}][{{$key}}][small]'
                                       value="{{$params['side']['option'][$i][$key]['small']?? ''}}"
                                       placeholder="If Small"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-3">
                                <input type='text' class="form-control"
                                       name='details[side][option][{{$i}}][{{$key}}][size]'
                                       value="{{$params['side']['option'][$i][$key]['size']?? ''}}"
                                       placeholder="Size x-y"/>
                            </div>
                            <div style="float: left;padding-top: 5px">
                                <b>:</b>
                            </div>
                            <div class="col-md-8">
                                <input type='text' class="form-control"
                                       name='details[side][option][{{$i}}][{{$key}}][standard]'
                                       value="{{$params['side']['option'][$i][$key]['standard']?? ''}}"
                                       placeholder="If Standard"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-12">
                                <input type='text' class="form-control"
                                       name='details[side][option][{{$i}}][{{$key}}][big]'
                                       value="{{$params['side']['option'][$i][$key]['big']?? ''}}"
                                       placeholder="If Big"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group  col-md-12">
                        <hr style="border-top: 1px solid #e4eaec">
                    </div>
                @endforeach
            @endif
        @endforeach
    @endif
@endif
