@extends('layouts.app')

@section('style')
    <link rel="stylesheet" href="{{ asset('css/component.css') }}"/>
    <link rel="stylesheet" href="{{ asset('js/modernizr.custom.js') }}"/>
@stop
@section('content')
    <!-- FIRST SECTION -->
    <section class="header__options mb-5">
        <!-- =================== NAVIGATION ======================= -->
        <header>
            <div class="container-fluid d-flex d-md-block flex-column">

                @include('layouts.search')
                @include('layouts.navbar')


            </div>
            <div class="container mt-sm-5 mt-3">
                <div class="row pl-md-3  align-items-md-center under__header">
                    <div class="logo col-md-2 col-3">
                        <a href="/">
                            <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="">
                        </a>

                    </div>
                    <div class="heading__text col-md-10 col-9 d-md-block d-flex align-items-end">
                        <h1 class="registration text-center">Registration Form for NEW USER </h1>
                    </div>
                </div>
            </div>
        </header>
        <!-- =================== REGISTER FORM ======================= -->
        <div class="container mt-4">
            <div class="row">
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('auth.register') }}"
                      class="text-center w-100 ac-custom ac-radio ac-fill">
                    @csrf

                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro {{ $errors->has('user_name') ? ' is-invalid' : '' }}"
                               type="text" id="input-8" name="user_name" value="{{ old('user_name') }}"
                               required/>
                        <label class="input__label input__label--kuro" for="input-8">
                            <span class="input__label-content input__label-content--kuro">{{ __('generic.user_name') }}</span>
                        </label>
                        @if ($errors->has('user_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('user_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="input input--kuro mb-3">
                        <select id="race-name" name="race_name" type="text" class="input__field input__field--kuro {{ $errors->has('race_name') ? ' is-invalid' : '' }}" required>
                            <option value="" disabled selected>Select Your Race</option>
                            @foreach($races as $race)
                                <option {{  old('race_name') == $race->id ? 'selected' : ''  }} value="{{$race->id}}">{{$race->race}}</option>
                            @endforeach
                        </select>
                        <label class="input__label input__label--kuro" for="input-7">
                            <span class="input__label-content input__label-content--kuro">{{ __('generic.race_name') }}</span>
                        </label>
                        @if ($errors->has('race_name'))
                            <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $errors->first('race_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <ul class="own__radio d-flex justify-content-between">
                        <!--<li class="w-25"></li>-->
                        <li>
                            <input id="r1" name="gender" value="male"
                                   type="radio" {{!old('gender')  || old('gender') != 'female' ?'checked':''}}/>
                            <label for="r1" class="">
                                <span class="d-block d-md-inline-block">{{ __('generic.male') }}</span>
                                <img src="{{ asset('storage/img/male.png') }}" alt="" width="60px"
                                     class="img-fluid"/>
                            </label>
                            @if(!old('gender')  || old('gender') != 'female')
                                <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809"
                                          style="stroke-dasharray: 499.664, 499.664; stroke-dashoffset: 0; transition: stroke-dashoffset 0.8s ease-in-out 0s;"></path>
                                </svg>
                            @endif
                        </li>
                        <li class="r2">
                            <input id="r2" name="gender" value="female"
                                   type="radio" {{old('gender') == 'female' ? 'checked' : '' }} />
                            <label for="r2" class="">
                                <img src="{{ asset('storage/img/female.png') }}" alt="" width="60px"
                                     class="img-fluid"/>
                                <span class="d-block d-md-inline-block">{{ __('generic.female') }}</span>
                            </label>
                            @if(old('gender') == 'female')
                                <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809"
                                          style="stroke-dasharray: 499.664, 499.664; stroke-dashoffset: 0; transition: stroke-dashoffset 0.8s ease-in-out 0s;"></path>
                                </svg>
                            @endif
                        </li>
                    </ul>


                    <div class="input input--kuro">

                        <select id="race-name" name="birthday" type="text" class="input__field input__field--kuro {{ $errors->has('birthday') ? ' is-invalid' : '' }}" required>
                            <option value="" disabled selected>{{ __('generic.birthday') }}</option>
                            @for($i = 5; $i<=100; $i++ )
                                <option {{  old('birthday') == $i ? 'selected' : ''  }} value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <label class="input__label input__label--kuro" for="input-9">
                            <span class="input__label-content input__label-content--kuro">{{ __('generic.birthday') }}</span>
                        </label>
                    </div>
                    <div class="mb-2">
                        <input class="" id="input-13" type="checkbox" name="agree" required/>
                        <label class="" for="input-13">I Agree To The Terms And Conditions </label>
                    </div>
                    <div>
                        <button class="btn reg-btn w-25">{{ __('generic.register') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@stop

@section('script')
    <script src="{{ asset('js/classie.js') }}"></script>
    <script src="{{ asset('js/svgcheckbx.js') }}"></script>
    <script>
        (function () {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function () {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function () {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call(document.querySelectorAll(['select.input__field','input.input__field'])).forEach(function (inputEl) {
                // in case the input is already filled..
                if (inputEl.value.trim() !== '') {
                    classie.add(inputEl.parentNode, 'input--filled');
                }

                // events:
                inputEl.addEventListener('focus', onInputFocus);
                inputEl.addEventListener('blur', onInputBlur);
            });

            function onInputFocus(ev) {
                classie.add(ev.target.parentNode, 'input--filled');
            }

            function onInputBlur(ev) {
                if (ev.target.value.trim() === '') {
                    classie.remove(ev.target.parentNode, 'input--filled');
                }
            }
        })();
    </script>
@stop
