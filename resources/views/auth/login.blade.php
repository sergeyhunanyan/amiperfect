@extends('layouts.app')

@section('content')
    <!-- FIRST SECTION -->
    <section class="header__options mb-5">
        <!-- =================== NAVIGATION ======================= -->
        <header>
            <div class="container-fluid d-flex d-md-block flex-column">

                @include('layouts.search')
                @include('layouts.navbar')


            </div>
            <div class="container mt-sm-5 mt-3">
                <div class="row pl-md-3  align-items-md-center under__header">
                    <div class="logo col-md-2 col-3">
                        <a href="/">
                            <img src="{{ asset('storage/img/logo.png') }}" width="100%" alt="">
                        </a>

                    </div>
                    <div class="heading__text col-md-10 col-9 d-md-block d-flex align-items-end">
                        <h1 class="registration text-center">Registration Form for NEW USER </h1>
                    </div>
                </div>
            </div>
        </header>
    </section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <form action="{{ route('login') }}" data-status="login-page" method="POST"
                      class="text-center w-100 ac-custom ac-radio ac-fill login-modal-form login-form">
                    @csrf
                    {{--<div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="email" id="input-7" name="email" value="{{ old('email') }}" required/>
                        <label class="input__label input__label--kuro" for="input-7">
                            <span class="input__label-content input__label-content--kuro">@lang('generic.email')</span>
                        </label>
                    </div>
                    <div class="input input--kuro">
                        <input class="input__field input__field--kuro" type="password" id="input-11" name="password"
                               required/>
                        <label class="input__label input__label--kuro" for="input-11">
                            <span class="input__label-content input__label-content--kuro">@lang('generic.password')</span>
                        </label>
                    </div>

                    <div class="mb-2">
                        <input class="" id="input-13" type="checkbox" name="remember"/>
                        <label class="" for="input-13">@lang('generic.remembe_me')</label>
                    </div>

                    <button type="submit" class="btn btn reg-btn w-100">@lang('generic.login')</button>--}}
                    <div class="input__rows mt-2">
                        <input type="email" name="email"
                               class="text-center {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('email') }}" placeholder="@lang('generic.email') *" required>
                    </div>
                    <div class="input__rows mt-2">
                        <input type="password" name="password"
                               class="text-center {{ $errors->has('password') ? ' is-invalid' : '' }}"
                               placeholder="@lang('generic.password') *"
                               required>
                    </div>
                    <div class="mt-2 d-flex justify-content-between align-items-center">
                        <button type="submit" class="text-center login__button">Log in</button>
                        <span class="or">or</span>
                        <a href="{{ route('register') }}" data-target="login-page"
                           class="text-center login__button btn-reg">Register</a>
                    </div>
                    <div class="mt-2 d-flex justify-content-between align-items-center">
                        <a href="{{ route('password.request') }}" style="font-size: 13px;">
                            {{ __('generic.forgot') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
