<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'not_confirm' => 'You have successfully registered, it remains to confirm e-mail',
    'confirm_request' => 'A confirmation request was sent to the :email email address',
    'required_register' => 'if you want to register, you need to write an email and a password',
    'blocked' => 'Sorry! Your profile is blocked.',
    'login_or_register' => 'If you want to continue you need to log in'

];
