<?php
return[
  'jaw_line_correction' => 'Jaw line correction',
  'lip_injection' => 'Lip injection',
  'chin_correction' => 'Chin correction',
  'botox' => 'Botox',
  'cheek_fillers' => 'Cheek Fillers',
  'nose_correction' => 'Nose correction',
  'liquid_face_lift' => 'Liquid  face lift',
  'bite' => 'Bite',
];
