<?php
return [
  'send_success' => 'A confirmation email has been sent to :email email address',
  'email_verified' => 'Your email was successfully verified',
];