<?php
return [
  'unexplained_error' => 'Unknown error try again',
  'get_the_code' => 'Get the message once every 5 minutes! Usually message comes in 1-2 minutes!',
  'invalid_token' => 'Invalid token!',
];